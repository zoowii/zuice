Zuice
====
Yet another dependency inject framework on Java

## Usage

    // deploy to your local maven repository, and add dependency(replace x.y.z)
    <dependency>
        <groupId>com.zoowii</groupId>
        <artifactId>zuice</artifactId>
        <version>x.y.z</version>
    </dependency>


## Features

* 依赖注入
* 支持Spring Framework风格的注解式依赖注入声明和XML方式的声明,语法基本和Spring一致
* 方便扩展
* 支持普通的getBean,也支持在全局/线程/一次注入操作中的bean获取单例
* AOP


## Demo

Demo代码参考src/test/java/zuice/test目录下代码

    // xml式配置
    <?xml version='1.0' encoding='UTF-8' ?>
    <beans xmlns="http://www.zuice.org/schema/beans"
           xmlns:context="http://www.zuice.org/schema/context"
           xmlns:aop="http://www.zuice.org/schema/aop">
        <context:property-placeholder location="classpath:test.properties"/>
        <!--<context:component-scan base-package="zuice.test.impl">-->
        <!--</context:component-scan>-->
        <bean id="age_service" class="zuice.test.impl.AgeService" lazy-init="false" init-method="init" scope="singleton"/>
        <bean class="zuice.test.impl.PersonService" scope="thread">
            <!--<property name="ageService" ref="age_service"/>-->
            <property name="family" value="${family}"/>
        </bean>
        <bean class="zuice.test.impl.HelloService" scope="inject">
            <!--<property name="personService" ref="personService"/>-->
            <!--<constructor-arg ref=""/>-->
            <!--<constructor-arg value=""/>-->
        </bean>

        <bean id="logAdvice" class="zuice.test.aspects.LogAdvice"/>
        <aop:aspectj-autoproxy proxy-target-class="true"/>
        <aop:config>
            <aop:advisor advice-ref="logAdvice"/>
        </aop:config>
    </beans>

    // 注解式声明
    @Component
    @InjectScope
    public class HelloService implements IHelloService {
        @Autowired
        private IPersonService personService;

        @Inject
        public HelloService(@Qualified("personService") Provider<IPersonService> personServiceProvider) {
            System.out.println("constructor of HelloService, ");
            System.out.println(personServiceProvider.get().getDescription("zoowii"));
        }

        @Override
        public String hello(String name) {
            return String.format("Hello, %s, \nand description is %s", name, personService.getDescription(name));
        }
    }

    // 使用bean例子
    BeanFactory injector = ClassPathXmlConfigLoader.createInjector("test_beans.xml"); // Injector.newInstance();
    //        injector.addBeanBind(HelloService.class);
    //        injector.addBeanBind(PersonService.class);
    //        injector.addBeanBind("age_service", AgeService.class);
    //        injector.scanPackage("zuice.test.impl");
    Provider<IHelloService> helloProvider = injector.getProvider(IHelloService.class);
    IHelloService helloService = helloProvider.get();
    System.out.println(helloService.hello("world"));
    Assert.assertTrue(helloService.hello("world").length() > 0);