package zuice;

import java.lang.reflect.Method;

/**
 * Created by zoowii on 14/11/5.
 */
public class AopProxy {
    private final Object instance;
    private final Method before;
    private final Method after;

    public AopProxy(Object _instance, Method _before, Method _after) {
        this.instance = _instance;
        this.before = _before;
        this.after = _after;
    }

    public Object getInstance() {
        return instance;
    }

    public Method getBefore() {
        return before;
    }

    public Method getAfter() {
        return after;
    }
}
