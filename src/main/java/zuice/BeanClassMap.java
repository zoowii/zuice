package zuice;

import java.util.*;

/**
 * Created by zoowii on 2015/1/15.
 */
public class BeanClassMap implements Map<BeanKey, Class<?>> {
    private Map<BeanKey, Class<?>> _recordsMap = new HashMap<>();

    @Override
    public int size() {
        return _recordsMap.size();
    }

    @Override
    public Collection<Class<?>> values() {
        return _recordsMap.values();
    }

    @Override
    public Class<?> get(Object key) {
        return _recordsMap.get(key); // TODO
    }

    @Override
    public Set<Entry<BeanKey, Class<?>>> entrySet() {
        return _recordsMap.entrySet();
    }

    @Override
    public void clear() {
        _recordsMap.clear();
    }

    @Override
    public Class<?> put(BeanKey key, Class<?> value) {
        return _recordsMap.put(key, value);
    }

    @Override
    public Class<?> remove(Object key) {
        return _recordsMap.remove(key);
    }

    @Override
    public boolean containsKey(Object key) {
        return _recordsMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return _recordsMap.containsValue(value);
    }

    @Override
    public boolean isEmpty() {
        return _recordsMap.isEmpty();
    }

    @Override
    public void putAll(Map<? extends BeanKey, ? extends Class<?>> m) {
        _recordsMap.putAll(m);
    }

    @Override
    public Set<BeanKey> keySet() {
        return _recordsMap.keySet();
    }
}
