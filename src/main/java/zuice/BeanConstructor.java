package zuice;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationHandler;

import zuice.aop.SimpleJointPoint;
import zuice.utils.*;

/**
 * Created by zoowii on 14/11/5.
 */
public class BeanConstructor {
    protected final Class<?> cls;

    public BeanConstructor(Class<?> cls) {
        this.cls = cls;
    }

    public Class<?> getCls() {
        return cls;
    }

    public Object newInstance(Object[] args, Function<Object, Void> initializer) throws Exception {
        Constructor<?> constructor = BeanUtils.getConstructorWithNParameters(cls, args.length);
        if (constructor != null) {
            Object instance = ClassUtils.callConstructor(constructor, args);
            initializer.apply(instance);
            return instance;
        } else {
            return null;
        }
    }
}
