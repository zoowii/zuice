package zuice;

/**
 * Created by zoowii on 14/11/3.
 */
public interface BeanFactory {
    public void initialize();

    public void addBeanBind(Class<?> cls, BeanInfo beanInfo);

    public void addBeanBind(Class<?> cls);

    public void addBeanBind(String name, Class<?> cls);

    public boolean isBeanClassInstanced(Class<?> cls);

    public void addBeanBind(String name, Class<?> cls, BeanInfo beanInfo);

    public <T> T getBean(Class<? extends T> declareCls);

    public Object getBean(String name);

    public <T> javax.inject.Provider<T> getProvider(Class<? extends T> cls);

    public javax.inject.Provider<Object> getProvider(String name);

    public Class<?>[] getBeanClasses();

    public void close();

    public void setAutoDestroy(boolean autoDestroy);

    public boolean isAutoDestroy();
}
