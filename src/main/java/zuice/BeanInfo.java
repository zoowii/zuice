package zuice;

import zuice.exceptions.InjectRuntimeException;
import zuice.utils.Function;

import java.util.*;

/**
 * Created by zoowii on 14/10/29.
 */
public class BeanInfo {
    private List<BeanInfoValue> constructArgs;
    private Map<String, BeanInfoValue> properties;

    public BeanInfo(List<BeanInfoValue> constructArgs, Map<String, BeanInfoValue> properties) {
        this.constructArgs = constructArgs;
        this.properties = properties;
    }

    private Class<?> _componentClass;
    private BeanConstructor beanConstructor;
    private boolean isLazyBean = true;
    private String initMethod;
    private String destroyMethod;
    private boolean isSingleton;
    private boolean isInjectScope;
    private boolean isThreadScope;


    public Class<?> getComponentClass() {
        return _componentClass;
    }

    public void setComponentClass(Class<?> cls) {
        _componentClass = cls;
        beanConstructor = new BeanConstructor(cls);
    }

    public Class<?> componentClass() {
        return _componentClass;
    }

    public void setBeanConstructor(BeanConstructor beanConstructor) {
        this.beanConstructor = beanConstructor;
    }

    public BeanConstructor getBeanConstructor() {
        return beanConstructor;
    }

    public Object newInstance(Object[] args, Function<Object, Void> initializer) {
        try {
            return beanConstructor.newInstance(args, initializer);
        } catch (Exception e) {
            throw new InjectRuntimeException(e);
        }
    }

    public List<BeanInfoValue> getConstructArgs() {
        return constructArgs;
    }

    public Map<String, BeanInfoValue> getProperties() {
        return properties;
    }

    public BeanInfoValue getProperty(String name) {
        return properties.get(name);
    }

    public void addProperty(String name, BeanInfoValue beanInfoValue) {
        properties.put(name, beanInfoValue);
    }

    public void addConstructorArg(BeanInfoValue beanInfoValue) {
        constructArgs.add(beanInfoValue);
    }

    public static BeanInfo emptyBeanInfo() {
        return emptyBeanInfo(true);
    }

    public static BeanInfo emptyBeanInfo(boolean isLazyBean) {
        return emptyBeanInfo(isLazyBean, null);
    }

    public static BeanInfo emptyBeanInfo(boolean isLazyBean, String initMethod) {
        return emptyBeanInfo(isLazyBean, initMethod, null);
    }

    public static BeanInfo emptyBeanInfo(boolean isLazyBean, String initMethod, String destroyMethod) {
        BeanInfo beanInfo = new BeanInfo(new ArrayList<BeanInfoValue>(), new HashMap<String, BeanInfoValue>());
        beanInfo.isLazyBean = isLazyBean;
        beanInfo.initMethod = initMethod;
        beanInfo.destroyMethod = destroyMethod;
        return beanInfo;
    }

    public boolean isThreadScope() {
        return isThreadScope;
    }

    public void setThreadScope(boolean isThreadScope) {
        this.isThreadScope = isThreadScope;
    }

    public boolean isLazyBean() {
        return isLazyBean;
    }

    public void setLazyBean(boolean isLazyBean) {
        this.isLazyBean = isLazyBean;
    }

    public String getInitMethod() {
        return initMethod;
    }

    public void setInitMethod(String initMethod) {
        this.initMethod = initMethod;
    }

    public String getDestroyMethod() {
        return destroyMethod;
    }

    public void setDestroyMethod(String destroyMethod) {
        this.destroyMethod = destroyMethod;
    }

    public boolean isSingleton() {
        return isSingleton;
    }

    public void setSingleton(boolean isSingleton) {
        this.isSingleton = isSingleton;
    }

    public boolean isInjectScope() {
        return isInjectScope;
    }

    public void setInjectScope(boolean isInjectScope) {
        this.isInjectScope = isInjectScope;
    }
}
