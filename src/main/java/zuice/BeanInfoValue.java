package zuice;

/**
 * Bean中比如构造函数参数,属性的值
 * Created by zoowii on 14/10/29.
 */
public abstract class BeanInfoValue {
    public static class PrimitiveBeanInfoValue extends BeanInfoValue {
        private final Object value;

        public PrimitiveBeanInfoValue(Object value) {
            this.value = value;
        }

        public Object getValue() {
            return value;
        }
    }

    public static class RefBeanInfoValue extends BeanInfoValue {
        private final String ref;

        /**
         * @param ref 可能是bean name,也可能是bean的class的全路径
         */
        public RefBeanInfoValue(String ref) {
            this.ref = ref;
        }

        public String getRef() {
            return ref;
        }
    }
}
