package zuice;

import java.util.*;

import zuice.utils.ClassUtils;

/**
 * Created by zoowii on 14/10/29.
 */
public abstract class BeanKey {
    public static class NameBeanKey extends BeanKey {
        private final String name;

        public NameBeanKey(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        private static final Map<String, NameBeanKey> instances = new HashMap<>();

        public static NameBeanKey of(String name) {
            if (!instances.containsKey(name)) {
                instances.put(name, new NameBeanKey(name));
            }
            return instances.get(name);
        }
    }

    public static class DeclareClassBeanKey extends BeanKey {
        private final Class<?> cls;

        public DeclareClassBeanKey(Class<?> cls) {
            this.cls = cls;
        }

        public Class<?> getCls() {
            return cls;
        }

        private static final Map<Class<?>, DeclareClassBeanKey> instances = new HashMap<>();

        public static DeclareClassBeanKey of(Class<?> cls) {
            if (!instances.containsKey(cls)) {
                instances.put(cls, new DeclareClassBeanKey(cls));
            }
            return instances.get(cls);
        }

        public static boolean matchClassBeanKey(BeanKey beanKey, Object instance) {
            if (beanKey instanceof DeclareClassBeanKey) {
                DeclareClassBeanKey clsKey = (DeclareClassBeanKey) beanKey;
                return ClassUtils.isAssignableFrom(clsKey.getCls(), instance.getClass());
            } else {
                return false;
            }
        }
    }
}
