package zuice;

import javax.inject.Provider;

/**
 * Created by zoowii on 14/10/31.
 */
public class BeanProvider<T> implements Provider<T> {
    private final Injector injector;
    private final InjectSession injectSession;
    private final Class<?> beanClass;

    public BeanProvider(Injector injector, InjectSession injectSession, Class<?> beanClass) {
        this.injector = injector;
        this.injectSession = injectSession;
        this.beanClass = beanClass;
    }

    @Override
    @SuppressWarnings("all")
    public T get() {
        return (T) injector.getBean(injectSession, beanClass);
    }
}
