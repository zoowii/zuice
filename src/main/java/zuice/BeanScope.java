package zuice;

/**
 * Created by zoowii on 14/11/1.
 */
public abstract class BeanScope {
    public static class SingletonBeanScope extends BeanScope {
    }

    public static class InjectBeanScope extends BeanScope {
    }

    public static class ThreadBeanScope extends BeanScope {
    }

    public static class NoneBeanScope extends BeanScope {
    }

    public static final String SINGLETON = "singleton";
    public static final String INJECT = "inject";
    public static final String THREAD = "thread";
    public static final String NONE = "none";

    public static BeanScope of(String scope) {
        if (SINGLETON.equals(scope)) {
            return new SingletonBeanScope();
        } else if (INJECT.equals(scope)) {
            return new InjectBeanScope();
        } else if (THREAD.equals(scope)) {
            return new ThreadBeanScope();
        } else if (NONE.equals(scope)) {
            return new NoneBeanScope();
        } else {
            return new SingletonBeanScope();
        }
    }
}
