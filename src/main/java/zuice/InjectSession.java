package zuice;

import zuice.utils.Function2;

import java.util.*;

/**
 * 一次注入会话
 * getBean调用的子getBean使用同一个注入会话
 * 对于scope=singleton/inject/thread的类,每个注入会话中最多实例化一次
 * Created by zoowii on 14/10/31.
 */
public class InjectSession {

    private final Map<BeanKey, Class<?>> beanClasses = new HashMap<>();

    /**
     * 不用WeakReference是因为,如果是比如singleton/thread scope这种情况,如果用weakref, 没有其他应用时这里引用会失效,从而重复初始化.
     * 而如果是Inject scope的话,inject session被释放时这些引用还是会释放的
     */
    private final Map<Class<?>, Object> beanInstances = new HashMap<>();

    public void iterateProcessBeanInstances(Function2<Class<?>, Object, Boolean> processor) {
        for (Class<?> beanCls : beanInstances.keySet()) {
            Object instance = beanInstances.get(beanCls);
            processor.apply(beanCls, instance);
        }
    }

    public boolean containsBean(Object bean) {
        return beanInstances.values().contains(bean);
    }

    public void clean() {
        beanInstances.clear();
    }

    private Class<?> getBeanClass(BeanKey beanKey) {
        return beanClasses.get(beanKey);
    }

    public void putBean(BeanKey beanKey, Object bean) {
        synchronized (this) {
            if (!beanClasses.containsKey(beanKey)) {
                beanClasses.put(beanKey, bean.getClass());
            }
            Class<?> cls = beanClasses.get(beanKey);
            beanInstances.put(cls, bean);
        }
    }

    public Object get(BeanKey beanKey) {
        Class<?> cls = beanClasses.get(beanKey);
        if (cls != null) {
            return beanInstances.get(cls);
        }
        if (beanKey instanceof BeanKey.DeclareClassBeanKey) {
            BeanKey.DeclareClassBeanKey clsBeanKey = (BeanKey.DeclareClassBeanKey) beanKey;
            for (Object instance : beanInstances.values()) {
                if (BeanKey.DeclareClassBeanKey.matchClassBeanKey(clsBeanKey, instance)) {
                    return instance;
                }
            }
            return null;
        } else {
            return null;
        }
    }

    public Iterator<Object> iterateValues() {
        final Iterator<Object> iterator = beanInstances.values().iterator();
        return new Iterator<Object>() {
            @Override
            public Object next() {
                return iterator.next();
            }

            @Override
            public void remove() {
                iterator.remove();
            }

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }
        };
    }
}
