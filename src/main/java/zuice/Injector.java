package zuice;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import javax.inject.Named;
import javax.inject.Inject;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import zuice.annotations.*;

import org.apache.commons.lang3.StringUtils;
import zuice.utils.*;
import zuice.utils.Function;

/**
 * 注入器
 */
public class Injector implements BeanFactory {
    private static final Logger LOG = Logger.getLogger(Injector.class);
    
    public static Injector newInstance() {
        Injector instance = new Injector();
        instance.initialize();
        return instance;
    }

    private final MultiHashMap<String, Class<?>, BeanInfo> nameClassBeanBindings = new MultiHashMap<>();

    /**
     * 记录类型的scope
     */
    private final Map<Class<?>, BeanScope> beanClassScopes = new ConcurrentHashMap<>();

    private final InjectSession singletonInjectSession = new InjectSession();
    private final ThreadScopeHolderHolder threadScopeHolderHolder = new ThreadScopeHolderHolder();
    private final List<PendingAction> pendingActions = new ArrayList<>();
    
    public void callPendingActions(){
        for(PendingAction action:pendingActions){
            if(action!=null){
                try{
                    action.apply();
                }catch (Exception e){
                    LOG.error(e);
                }
            }
        }
    }

    /**
     * 哪些类注解表示是bean class
     */
    private final List<Class<?>> componentAnnotations = new ArrayList<>();

    public BeanInfo getBeanInfo(String name) {
        return nameClassBeanBindings.getByFirstKey(name);
    }

    @Override
    public Class<?>[] getBeanClasses() {
        Set<Class<?>> clsSet = beanClassScopes.keySet();
        Class<?>[] clsArray = new Class<?>[clsSet.size()];
        Iterator<Class<?>> clsIterator = clsSet.iterator();
        int i = 0;
        while (clsIterator.hasNext()) {
            Class<?> cls = clsIterator.next();
            clsArray[i] = cls;
            i += 1;
        }
        return clsArray;
    }

    /**
     * 遍历目前注入的bean infos
     *
     * @return
     */
    public Iterator<BeanInfo> iterateBeanInfos() {
        return nameClassBeanBindings.iterateValues();
    }

    @Override
    public void initialize() {
        addComponentAnnotation(Component.class);
        beanClassScopes.put(this.getClass(), BeanScope.of(BeanScope.SINGLETON));
        BeanInfo injectorBeanInfo = BeanInfo.emptyBeanInfo();
        nameClassBeanBindings.put(this.getClass().getCanonicalName(), this.getClass(), injectorBeanInfo);
        singletonInjectSession.putBean(BeanKey.DeclareClassBeanKey.of(BeanFactory.class), this);
    }

    public void addComponentAnnotation(Class<?> cls) {
        if (componentAnnotations.contains(cls)) {
            return;
        }
        synchronized (componentAnnotations) {
            if (componentAnnotations.contains(cls)) {
                return;
            }
            componentAnnotations.add(cls);
        }
    }

    /**
     * 判断是否是基本类型和对应封装类型,以及String, Date之外的类型
     *
     * @param cls
     * @return
     */
    private boolean isRefClassType(Class<?> cls) {
        if (cls == null)
            return false;
        else {
            if (cls.isPrimitive() || cls == Boolean.class
                    || cls == String.class || cls == Date.class || ClassUtils.isAssignableFrom(Number.class, cls)) {
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * 将类型的@Inject的属性信息注入到beanInfo中,如果原来没有的话
     *
     * @param cls
     * @param beanInfo
     * @return
     */
    private void loadInjectPropertiesToBeanInfo(Class<?> cls, BeanInfo beanInfo) {
        if (cls == null || beanInfo == null) {
            return;
        }
        // 找到所有的带有@Inject, @Autowired, 或者@Qualified 注解的属性(将来可能支持在getter/setter上找这些注解)
        Field[] fields = cls.getDeclaredFields();
        for (Field field : fields) {
            if (isRefClassType(field.getType())) {
                String refName = null;
                if (field.getAnnotation(Inject.class) != null || field.getAnnotation(Autowired.class) != null) {
                    Named named = field.getAnnotation(Named.class);
                    if (named != null && !StringUtils.isEmpty(named.value())) {
                        refName = named.value();
                    } else {
                        refName = field.getType().getName();
                    }
                } else if (field.getAnnotation(Qualified.class) != null) {
                    Qualified qualified = field.getAnnotation(Qualified.class);
                    if (!StringUtils.isEmpty(qualified.value())) {
                        refName = qualified.value();
                    }
                }
                if (refName != null) {
                    if (beanInfo.getProperty(field.getName()) != null) {
                        return;
                    }
                    BeanInfoValue.RefBeanInfoValue beanInfoValue = new BeanInfoValue.RefBeanInfoValue(refName);
                    beanInfo.addProperty(field.getName(), beanInfoValue);
                }
            } else {
                // TODO: 基本类型,另外注入,比如从xml中
            }
        }
    }

    private void loadInjectConstructorsToBeanInfo(Class<?> cls, BeanInfo beanInfo) {
        if (cls == null || beanInfo == null) {
            return;
        }
        if (beanInfo.getConstructArgs().size() > 0) {
            return;
        }
        // 找到一个带有@Inject的构造函数,不止有超过1个构造函数带有@Inject注解(将来可能支持,需要修改BeanInfo的结构)
        Constructor<?>[] constructors = cls.getConstructors();
        boolean constructorBeanInfoInited = false;
        for (Constructor<?> constructor : constructors) {
            if (!constructorBeanInfoInited && constructor.getAnnotation(Inject.class) != null) {
                int paramsCount = constructor.getParameterTypes().length;
                for (int i = 0; i < paramsCount; ++i) {
                    Class<?> paramType = constructor.getParameterTypes()[i];
                    Annotation[] paramAnnotations = constructor.getParameterAnnotations()[i];
                    String refName = null;
                    Named named = (Named) ListUtils.firstInAnnotationArrayOfClass(paramAnnotations, Named.class);
                    if (named != null && !StringUtils.isEmpty(named.value())) {
                        refName = named.value();
                    }
                    Qualified qualified = (Qualified) ListUtils.firstInAnnotationArrayOfClass(paramAnnotations, Qualified.class);
                    if (qualified != null && !StringUtils.isEmpty(qualified.value())) {
                        refName = qualified.value();
                    }
                    if (refName == null) {
                        refName = paramType.getName();
                    }
                    BeanInfoValue.RefBeanInfoValue beanInfoValue = new BeanInfoValue.RefBeanInfoValue(refName);
                    beanInfo.addConstructorArg(beanInfoValue);
                }
                constructorBeanInfoInited = true;
            }
        }
    }

    /**
     * 从类型注解中自动找到这个类型bean的bean info
     *
     * @param cls
     * @return
     */
    private BeanInfo getBeanInfoFromClassAnnotation(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        BeanInfo beanInfo = new BeanInfo(new ArrayList<BeanInfoValue>(), new HashMap<String, BeanInfoValue>());
        loadInjectPropertiesToBeanInfo(cls, beanInfo);
        loadInjectConstructorsToBeanInfo(cls, beanInfo);
        // 设置scope
        if (isSingletonBeanClassOfItself(cls)) {
            beanInfo.setSingleton(true);
        } else if (isThreadScopeComponentOfItself(cls)) {
            beanInfo.setThreadScope(true);
        } else if (isInjectScopeBeanClassOfItself(cls)) {
            beanInfo.setInjectScope(true);
        }
        return beanInfo;
    }

    /**
     * 扫描某个包,加载其中所有使用了类似@Component这种在componentAnnotations中的注解的类作为bean class
     *
     * @param packagePath
     */
    public void scanPackage(String packagePath) {
        List<Class<?>> classes = ClassUtils.findClassesInPackage(packagePath);
        List<Pair<Class<?>, Annotation>> classesToAdd = ClassUtils.filterClassWithAnyAnnotationsInList(classes, componentAnnotations);
        ListUtils.map(classesToAdd, new zuice.utils.Function<Pair<Class<?>, Annotation>, Void>() {
            @Override
            public Void apply(Pair<Class<?>, Annotation> pair) {
                Class<?> cls = pair.getLeft();
                Annotation annotation = pair.getRight();
                Object name = BeanUtils.getPropertyOfAnnotation(annotation, "value");
                if (name == null || StringUtils.isEmpty(name.toString())) {
                    addBeanBind(cls);
                } else {
                    addBeanBind(name.toString(), cls);
                }
                return null;
            }
        });
    }

    /**
     * 注册bean class的scope信息
     *
     * @param cls
     * @param beanInfo
     */
    private void registerBeanClassScope(Class<?> cls, BeanInfo beanInfo) {
        if (cls == null || beanInfo == null) {
            return;
        }
        if (beanInfo.isSingleton())
            beanClassScopes.put(cls, BeanScope.of(BeanScope.SINGLETON));
        else if (beanInfo.isThreadScope())
            beanClassScopes.put(cls, BeanScope.of(BeanScope.THREAD));
        else if (beanInfo.isInjectScope())
            beanClassScopes.put(cls, BeanScope.of(BeanScope.INJECT));
        else {
            if (isSingletonBeanClassOfItself(cls))
                beanClassScopes.put(cls, BeanScope.of(BeanScope.SINGLETON));
            else if (isThreadScopeComponentOfItself(cls))
                beanClassScopes.put(cls, BeanScope.of(BeanScope.THREAD));
            else if (isInjectScopeBeanClassOfItself(cls))
                beanClassScopes.put(cls, BeanScope.of(BeanScope.INJECT));
            else
                beanClassScopes.put(cls, BeanScope.of(null));
        }
    }

    public void addBeanBind(final Class<?> cls, BeanInfo beanInfo, boolean lazyCreate){
        if (cls == null || beanInfo == null) {
            return;
        }
        beanInfo.setComponentClass(cls);
        synchronized (nameClassBeanBindings) {
            nameClassBeanBindings.putBySecondKey(cls, beanInfo);
            registerBeanClassScope(cls, beanInfo);
            loadInjectConstructorsToBeanInfo(cls, beanInfo);
            loadInjectPropertiesToBeanInfo(cls, beanInfo);
            if (!beanInfo.isLazyBean() && !isBeanClassInstanced(cls)) {
                if(lazyCreate){
                    pendingActions.add(new PendingAction() {
                        @Override
                        public void apply() {
                            getBean(cls);
                        }
                    });
                } else {
                    getBean(cls);
                }
            }
        }
    }

    @Override
    public void addBeanBind(Class<?> cls, BeanInfo beanInfo) {
        addBeanBind(cls, beanInfo, false);
    }

    @Override
    public void addBeanBind(Class<?> cls) {
        if (cls == null) {
            return;
        }
        addBeanBind(cls, getBeanInfoFromClassAnnotation(cls));
    }

    @Override
    public void addBeanBind(String name, Class<?> cls) {
        if (StringUtils.isEmpty(name) || cls == null) {
            return;
        }
        addBeanBind(name, cls, getBeanInfoFromClassAnnotation(cls));
    }

    private Iterator<Object> iterateBeanInstanceValues() {
        return singletonInjectSession.iterateValues();
    }

    /**
     * 判断bean class是否实例化过
     *
     * @param cls
     * @return
     */
    @Override
    public boolean isBeanClassInstanced(Class<?> cls) {
        if (cls == null) {
            return false;
        } else {
            Iterator<Object> instancesIterator = iterateBeanInstanceValues();
            while (instancesIterator.hasNext()) {
                Object instance = instancesIterator.next();
                if (instance != null && cls.isInstance(instance)) {
                    return true;
                }
            }
            return false;
        }
    }
    
    public void addBeanBind(final String name, Class<?> cls, BeanInfo beanInfo, boolean lazyCreate){
        if (StringUtils.isEmpty(name) || cls == null || beanInfo == null) {
            return;
        }
        beanInfo.setComponentClass(cls);
        synchronized (nameClassBeanBindings) {
            nameClassBeanBindings.put(name, cls, beanInfo);
            registerBeanClassScope(cls, beanInfo);
            loadInjectConstructorsToBeanInfo(cls, beanInfo);
            loadInjectPropertiesToBeanInfo(cls, beanInfo);
            if (!beanInfo.isLazyBean() && !isBeanClassInstanced(cls)) {
                if(lazyCreate){
                    pendingActions.add(new PendingAction() {
                        @Override
                        public void apply() {
                            getBean(name);
                        }
                    });
                } else {
                    getBean(name);
                }
            }
        }
    }

    @Override
    public void addBeanBind(String name, Class<?> cls, BeanInfo beanInfo) {
         addBeanBind(name, cls, beanInfo, false);
    }

    /**
     * 类型本身是否有singleton注解
     *
     * @param cls
     * @return
     */
    private boolean isSingletonBeanClassOfItself(Class<?> cls) {
        return ClassUtils.getAnnotationOfClass(cls, javax.inject.Singleton.class) != null;
    }

    private boolean isSingletonBeanClass(Class<?> cls) {
        BeanScope scope = beanClassScopes.get(cls);
        return scope != null && scope instanceof BeanScope.SingletonBeanScope;
    }

    /**
     * 类型本身是否有inject-scope信息
     *
     * @param cls
     * @return
     */
    private boolean isInjectScopeBeanClassOfItself(Class<?> cls) {
        return cls != null && cls.getAnnotation(InjectScope.class) != null;
    }

    /**
     * 判断是否是在一次注入会话中最多实例化一次的类型
     *
     * @param cls
     * @return
     */
    private boolean isInjectScopeBeanClass(Class<?> cls) {
        BeanScope scope = beanClassScopes.get(cls);
        return scope != null && scope instanceof BeanScope.InjectBeanScope;
    }

    /**
     * 类型本身是否有thread-scope信息
     *
     * @param cls
     * @return
     */
    private boolean isThreadScopeComponentOfItself(Class<?> cls) {
        return cls != null && cls.getAnnotation(zuice.annotations.ThreadScope.class) != null;
    }

    private boolean isThreadScopeComponent(Class<?> cls) {
        BeanScope scope = beanClassScopes.get(cls);
        return scope != null && scope instanceof BeanScope.ThreadBeanScope;
    }

    private void updateBeanInstances(InjectSession session, BeanKey beanKey, Object bean) {
        singletonInjectSession.putBean(beanKey, bean);
        Class<?> beanRealCls = ClassUtils.getRealClassOverProxyAdvised(bean);
        threadScopeHolderHolder.getHolder(beanRealCls).set(bean);
        session.putBean(beanKey, bean);
    }

    private Object getBeanFromBeanInstanceCache(BeanKey beanKey) {
        return singletonInjectSession.get(beanKey);
    }

    /**
     * 从声明类型自动获取到bean
     * 要判断是否是singleson的实例,如果是,最多存在一个实例,如果不是,每次创建一个新实例
     *
     * @param declareCls 声明类型可能是父类型或实现的接口.用Class::assignable来判断
     */
    private Object justGetBean(InjectSession session, Class<?> declareCls) {
        Object[] keys = nameClassBeanBindings.secondKeys().toArray();
        for (Object key : keys) {
            Class<?> beanCls = (Class<?>) key;
            if (ClassUtils.isAssignableFrom(declareCls, beanCls)) {
                BeanKey beanKey = BeanKey.DeclareClassBeanKey.of(beanCls);
                if (isSingletonBeanClass(beanCls) && getBeanFromBeanInstanceCache(beanKey) != null) {
                    return getBeanFromBeanInstanceCache(beanKey);
                }
                if (isInjectScopeBeanClass(beanCls) && session.get(beanKey) != null) {
                    return session.get(beanKey);
                }
                ThreadScopeHolderHolder.ThreadScopeHolder scopeHolder = threadScopeHolderHolder.getHolder(beanCls);
                if (isThreadScopeComponent(beanCls) && scopeHolder.get() != null) {
                    return scopeHolder.get();
                }
                BeanInfo beanInfo = nameClassBeanBindings.getBySecondKey(beanCls);
                Object bean = createBean(session, beanKey, beanCls, beanInfo);
                updateBeanInstances(session, beanKey, bean);
                return bean;
            }
        }
        return null;
    }

    /**
     * @param name
     * @return
     */
    private Object justGetBean(InjectSession session, String name) {
        if (!nameClassBeanBindings.containsFirstKey(name)) {
            return null;
        }
        BeanInfo beanInfo = nameClassBeanBindings.getByFirstKey(name);
        Class<?> beanCls = beanInfo.getComponentClass();
        BeanKey beanKey = BeanKey.NameBeanKey.of(name);
        if (isSingletonBeanClass(beanCls) && getBeanFromBeanInstanceCache(beanKey) != null) {
            return getBeanFromBeanInstanceCache(beanKey);
        }
        if (isInjectScopeBeanClass(beanCls) && session.get(beanKey) != null) {
            return session.get(beanKey);
        }
        ThreadScopeHolderHolder.ThreadScopeHolder scopeHolder = threadScopeHolderHolder.getHolder(beanCls);
        if (isThreadScopeComponent(beanCls) && scopeHolder.get() != null) {
            return scopeHolder.get();
        }
        Object bean = createBean(session, beanKey, beanCls, beanInfo);
        if (isThreadScopeComponent(beanCls)) {
            scopeHolder.set(bean);
        }
        updateBeanInstances(session, beanKey, bean);
        return bean;
    }

    @Override
    public <T> T getBean(Class<? extends T> declareCls) {
        return getBean(new InjectSession(), declareCls);
    }

    /**
     * 先尝试从类型获取,再尝试从名称获取
     *
     * @param session
     * @param declareCls
     * @return
     */
    @SuppressWarnings("all")
    public <T> T getBean(InjectSession session, Class<? extends T> declareCls) {
        Object bean = justGetBean(session, declareCls);
        if (bean != null) {
            return (T) bean;
        } else {
            String clsName = declareCls.getSimpleName();
            String name = (clsName.charAt(0) + "").toLowerCase() + clsName.substring(1);
            Object beanFromClassName = justGetBean(session, name);
            if (beanFromClassName != null)
                return (T) beanFromClassName;
            else {
                Iterator<String> namedBeanClassPairKeyIterator = nameClassBeanBindings.iterateFirstKeys();
                while (namedBeanClassPairKeyIterator.hasNext()) {
                    String name1 = namedBeanClassPairKeyIterator.next();
                    BeanInfo beanInfo = nameClassBeanBindings.getByFirstKey(name1);
                    Class<?> cls = beanInfo.getComponentClass();
                    if (declareCls == null || cls == null) {
                        return null;
                    }
                    if (ClassUtils.isAssignableFrom(declareCls, cls)) {
                        return (T) justGetBean(session, name1);
                    }
                }
                return null;
            }
        }
    }

    @Override
    public Object getBean(String name) {
        return getBean(new InjectSession(), name);
    }
    
    /**
     * 先尝试从名称获取,再尝试从类型获取
     *
     * @param name
     * @return
     */
    public Object getBean(InjectSession session, String name) {
        Object bean = justGetBean(session, name);
        if (bean != null) {
            return bean;
        } else {
            Object[] keys = nameClassBeanBindings.secondKeys().toArray();
            for (Object key : keys) {
                Class<?> cls = (Class<?>) key;
                String clsName = cls.getSimpleName();
                if (clsName.equalsIgnoreCase(name)) {
                    return justGetBean(session, cls);
                }
            }
            return null;
        }
    }

    @Override
    public <T> javax.inject.Provider<T> getProvider(Class<? extends T> cls) {
        return getProvider(new InjectSession(), cls);
    }

    @Override
    public javax.inject.Provider<Object> getProvider(String name) {
        return getProvider(new InjectSession(), name);
    }

    private <T> javax.inject.Provider<T> getProvider(InjectSession session, Class<? extends T> cls) {
        return new BeanProvider<>(this, session, cls);
    }

    private javax.inject.Provider<Object> getProvider(InjectSession session, String name) {
        return new NameBeanProvider<>(this, session, name);
    }

    private Object getBeanInfoValue(InjectSession session, BeanInfoValue beanInfoValue, Class<?> beanInfoValueType) {
        Object argValue;
        if (beanInfoValue instanceof BeanInfoValue.PrimitiveBeanInfoValue) {
            argValue = ((BeanInfoValue.PrimitiveBeanInfoValue) beanInfoValue).getValue();
            return ClassUtils.parseToPrimitiveTypeValue(beanInfoValueType, argValue);
        } else if (beanInfoValue instanceof BeanInfoValue.RefBeanInfoValue) {
            BeanInfoValue.RefBeanInfoValue refValue = (BeanInfoValue.RefBeanInfoValue) beanInfoValue;
            String ref = refValue.getRef();
            if (ClassUtils.isAssignableFrom(javax.inject.Provider.class, beanInfoValueType)) {
                argValue = getProvider(session, ref); // FIXME: 如果ref以Provider结尾且前面有其他内容,去掉这个Provider
            } else {
                Object nameBeanValue = getBean(session, ref);
                if (nameBeanValue != null) {
                    argValue = nameBeanValue;
                } else {
                    try {
                        Class<?> cls = this.getClass().getClassLoader().loadClass(ref);
                        if (cls == null) {
                            throw new RuntimeException("Can't load class " + ref);
                        }
                        argValue = getBean(session, cls);
                    } catch (ClassNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
            return argValue;
        } else {
            throw new RuntimeException("Wrong arg value format");
        }
    }

    /**
     * 构造bean的实例
     *
     * @param beanKey
     * @param cls
     * @param beanInfo
     */
    private Object createBean(final InjectSession session, final BeanKey beanKey, final Class<?> cls, final BeanInfo beanInfo) {
        if (isSingletonBeanClass(cls) && getBeanFromBeanInstanceCache(beanKey) != null) {
            return getBeanFromBeanInstanceCache(beanKey);
        }
        if (isInjectScopeBeanClass(cls) && session.get(beanKey) != null) {
            return session.get(beanKey);
        }
        ThreadScopeHolderHolder.ThreadScopeHolder scopeHolder = threadScopeHolderHolder.getHolder(cls);
        if (isThreadScopeComponent(cls) && scopeHolder.get() != null) {
            return scopeHolder.get();
        }
        Constructor<?> constructor = BeanUtils.getConstructorWithNParameters(cls, beanInfo.getConstructArgs().size());
        if (constructor == null) {
            return null;
        }
        List<BeanInfoValue> constructArgInfos = beanInfo.getConstructArgs();
        List<Object> constructArgs = new ArrayList<>();
        Class<?>[] constructorParameterTypes = constructor.getParameterTypes();
        for (int i = 0; i < constructArgInfos.size(); ++i) {
            BeanInfoValue argInfo = constructArgInfos.get(i);
            Object argValue = getBeanInfoValue(session, argInfo, constructorParameterTypes[i]);
            constructArgs.add(argValue);
        }
        Function<Object, Void> initializer = new Function<Object, Void>() {
            @Override
            public Void apply(Object obj) {
                updateBeanInstances(session, beanKey, obj);
                // set properties
                Object[] keys = beanInfo.getProperties().keySet().toArray();
                for (Object key : keys) {
                    String propertyName = (String) key;
                    BeanInfoValue propertyInfo = beanInfo.getProperties().get(propertyName);
                    if (propertyInfo != null) {
                        FieldAccessor fieldAccessor = new FieldAccessor(cls, propertyName);
                        Object value = getBeanInfoValue(session, propertyInfo, fieldAccessor.getPropertyType());
                        fieldAccessor.setProperty(obj, value);
                    }
                }
                return null;
            }
        };
        Object bean = beanInfo.newInstance(constructArgs.toArray(), initializer);
        //    // set properties
        //    val keys = beanInfo.getProperties().keySet().toArray
        //    for (i <- 0 until keys.length) {
        //      val propertyName = keys(i).asInstanceOf[String]
        //      val propertyInfo = beanInfo.getProperties().get(propertyName)
        //      if (propertyInfo != null) {
        //        val fieldAccessor = new FieldAccessor(cls, propertyName)
        //        val value = getBeanInfoValue(session, propertyInfo, fieldAccessor.getPropertyType)
        //        fieldAccessor.setProperty(bean, value)
        //      }
        //    }
        // call init-method if exists
        if (beanInfo.getInitMethod() != null) {
            Method initMethod = ClassUtils.getDeclaredMethod(cls, beanInfo.getInitMethod());
            if (initMethod != null) {
                ClassUtils.invokeMethod(initMethod, bean);
            }
        }
        return bean;
    }

    @Override
    public void close() {
        //clean up the beans, may call destroy-method if the beanFactory set auto-destroy before
        if (!isAutoDestroy()) {
            return;
        }
        Function2<Class<?>, Object, Boolean> cleanProcessor = new Function2<Class<?>, Object, Boolean>() {
            @Override
            public Boolean apply(Class<?> beanCls, Object instance) {
                BeanInfo beanInfo = nameClassBeanBindings.getBySecondKey(beanCls);
                if (beanInfo != null && beanInfo.getDestroyMethod() != null && instance != null) {
                    try {
                        ClassUtils.callMethod(ClassUtils.getMethodByName(beanCls, beanInfo.getDestroyMethod()), instance, new Object[0]);
                    } catch (Exception e) {
                    }
                }
                return true;
            }
        };
        singletonInjectSession.iterateProcessBeanInstances(cleanProcessor);
        for (ThreadScopeHolderHolder.ThreadScopeHolder holder : threadScopeHolderHolder.getAllThreadScopeHolders()) {
            try {
                Object obj = holder.get();
                if (obj != null) {
                    if (!singletonInjectSession.containsBean(obj)) {
                        BeanInfo beanInfo = nameClassBeanBindings.getBySecondKey(obj.getClass());
                        if (beanInfo != null && beanInfo.getDestroyMethod() != null) {
                            try {
                                ClassUtils.callMethod(ClassUtils.getMethodByName(obj.getClass(), beanInfo.getDestroyMethod()), obj, new Object[0]);
                            } catch (Exception e) {
                            }
                        }
                    }
                    holder.remove();
                }
            } catch (Exception e) {
            }
        }
        singletonInjectSession.clean();
    }

    private boolean _autoDestroy = true;

    @Override
    public void setAutoDestroy(boolean autoDestroy) {
        this._autoDestroy = autoDestroy;
    }

    @Override
    public boolean isAutoDestroy() {
        return _autoDestroy;
    }
}
