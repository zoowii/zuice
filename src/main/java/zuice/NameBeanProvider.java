package zuice;

import javax.inject.Provider;

/**
 * Created by zoowii on 14/10/31.
 */
public class NameBeanProvider<T> implements Provider<T> {
    private final Injector injector;
    private final InjectSession injectSession;
    private final String name;

    public NameBeanProvider(Injector injector, InjectSession injectSession, String name) {
        this.injector = injector;
        this.injectSession = injectSession;
        this.name = name;
    }

    @Override
    @SuppressWarnings("all")
    public T get() {
        return (T) injector.getBean(injectSession, name);
    }
}
