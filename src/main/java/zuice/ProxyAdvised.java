package zuice;

import zuice.utils.ClassUtils;

import java.lang.reflect.Method;

/**
 * Created by zoowii on 2015/1/15.
 */
public interface ProxyAdvised {
    public Class<?> getTargetClass();

    public Object getProxyInstance();

    public static final Method getTargetClassMethod = ClassUtils.getMethodByName(ProxyAdvised.class, "getTargetClass");
    public static final Method getProxyInstanceMethod = ClassUtils.getMethodByName(ProxyAdvised.class, "getProxyInstance");
}
