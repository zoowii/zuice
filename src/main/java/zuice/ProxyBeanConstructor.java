package zuice;

import java.lang.reflect.*;
import java.util.List;

import zuice.aop.SimpleJointPoint;
import zuice.utils.*;

/**
 * Created by zoowii on 14/11/5.
 */
public class ProxyBeanConstructor extends BeanConstructor {
    private final AopProxy aopProxy;
    private final BeanConstructor originBeanConstructor;

    public ProxyBeanConstructor(Class<?> cls, AopProxy aopProxy, BeanConstructor originBeanConstructor) {
        super(cls);
        this.aopProxy = aopProxy;
        this.originBeanConstructor = originBeanConstructor;
    }

    private Object createOriginInstance(Object[] args, Function<Object, Void> initializer) throws Exception {
        if (originBeanConstructor != null) {
            return originBeanConstructor.newInstance(args, initializer);
        } else {
            return new BeanConstructor(cls).newInstance(args, initializer);
        }
    }

    @Override
    public Object newInstance(Object[] args, Function<Object, Void> initializer) throws Exception {
        final Object origin = createOriginInstance(args, initializer);
        // 添加一个post-handler,用来对origin对象进行执行构造函数之后的操作,比如初始化属性成员
        initializer.apply(origin);
        InvocationHandler invocationHandler = new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws InvocationTargetException, IllegalAccessException {
                if (method.equals(ProxyAdvised.getTargetClassMethod)) {
                    return cls;
                }
                if (method.equals(ProxyAdvised.getProxyInstanceMethod)) {
                    return origin;
                }
                SimpleJointPoint joinPoint = new SimpleJointPoint(proxy, method, args);
                Object[] aopArgs = new Object[1];
                aopArgs[0] = joinPoint;
                // 这里不把before-method放入try-catch块里是为了可以用在before-method中抛出异常来达到不执行目标method的目的
                // 从而间接实现interceptor的功能
                ClassUtils.callMethod(aopProxy.getBefore(), aopProxy.getInstance(), aopArgs);
                try {
                    return ClassUtils.callMethod(method, origin, args);
                } finally {
                    try {
                        ClassUtils.callMethod(aopProxy.getAfter(), aopProxy.getInstance(), aopArgs);
                    } catch (Exception e) {

                    }
                }
            }
        };
        List<Class<?>> interfaceList = ListUtils.concat(cls.getInterfaces(), new Class<?>[]{ProxyAdvised.class});
        Class<?>[] interfaces = new Class<?>[interfaceList.size()];
        interfaceList.toArray(interfaces);
        return Proxy.newProxyInstance(cls.getClassLoader(), interfaces, invocationHandler);
    }
}
