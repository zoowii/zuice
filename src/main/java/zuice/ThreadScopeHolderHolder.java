package zuice;

import java.util.*;

/**
 * 管理thread scope的holder的holder
 */
public class ThreadScopeHolderHolder {
    private final Map<Class<?>, ThreadScopeHolder> typeHolderMap = new java.util.HashMap<>();

    public ThreadScopeHolder getHolder(Class<?> cls) {
        synchronized (typeHolderMap) {
            if (!typeHolderMap.containsKey(cls)) {
                typeHolderMap.put(cls, new ThreadScopeHolder());
            }
            return typeHolderMap.get(cls);
        }
    }

    public ThreadScopeHolder[] getAllThreadScopeHolders() {
        Object[] holdersObjArray = typeHolderMap.values().toArray();
        ThreadScopeHolder[] holders = new ThreadScopeHolder[(holdersObjArray.length)];
        for (int i = 0; i < holdersObjArray.length; ++i) {
            holders[i] = (ThreadScopeHolder) holdersObjArray[i];
        }
        return holders;
    }

    public static class ThreadScopeHolder {
        private ThreadLocal<Object> scopeObjectThreadLocals = new ThreadLocal<>();

        public void set(Object obj) {
            scopeObjectThreadLocals.set(obj);
        }

        public Object get() {
            return scopeObjectThreadLocals.get();
        }

        public void remove() {
            scopeObjectThreadLocals.remove();
        }
    }
}
