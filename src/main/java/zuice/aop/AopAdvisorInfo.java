package zuice.aop;

import java.lang.reflect.Method;
import java.util.*;

import zuice.AopProxy;
import zuice.utils.StringUtils;

/**
 * advisor类的信息,包括有哪些pointcut和对应的触发器,before/after等
 * Created by zoowii on 14/11/5.
 */
public class AopAdvisorInfo {
    private Map<PointcutInfo, List<AspectMethodInfo>> _pointcuts = new HashMap<>();

    public void addPointcutIfNotExists(PointcutInfo pointcutInfo) {
        if (pointcutInfo != null) {
            if (!_pointcuts.containsKey(pointcutInfo)) {
                _pointcuts.put(pointcutInfo, new ArrayList<AspectMethodInfo>());
            }
        }
    }

    public List<AspectMethodInfo> getPointcutAspects(PointcutInfo pointcutInfo) {
        if (pointcutInfo != null)
            return _pointcuts.get(pointcutInfo);
        else
            return null;
    }

    /**
     * 找到pointcut作用的包/类名前缀(全名),满足这个条件的包中的类都需要,多个作用包(类)前缀用空格分隔
     * 支持用*作为通配符
     *
     * @param pointcutInfo
     * @return
     */
    public String[] getPointcutExecutionClassPrefixes(PointcutInfo pointcutInfo) {
        if (pointcutInfo == null || StringUtils.isEmpty(pointcutInfo.getExpression())) {
            return null;
        }
        String exprs = pointcutInfo.getExpression();
        List<String> prefixes = new ArrayList<>();
        String[] splited = exprs.split("\\s"); // TODO: maybe use \\s|, is better???
        for (String expr : splited) {
            prefixes.add(StringUtils.trim(StringUtils.trim(expr, "*"), "."));
        }
        String[] prefixArray = new String[prefixes.size()];
        prefixes.toArray(prefixArray);
        return prefixArray;
    }

    public Iterator<AspectMethodInfo> iteratePointcutAspects(PointcutInfo pointcutInfo) {
        List<AspectMethodInfo> methodInfos = getPointcutAspects(pointcutInfo);
        if (methodInfos != null)
            return methodInfos.iterator();
        else
            return null;
    }

    public AopProxy createAopProxyFromPointcut(Object advisorInstance, PointcutInfo pointcutInfo) {
        Method beforeMethod = null;
        Method afterMethod = null;
        Iterator<AspectMethodInfo> aspectMethodInfoIterator = iteratePointcutAspects(pointcutInfo);
        if (aspectMethodInfoIterator == null) {
            return null;
        }
        while (aspectMethodInfoIterator.hasNext()) {
            AspectMethodInfo aspectMethodInfo = aspectMethodInfoIterator.next();
            if (aspectMethodInfo.isBefore()) {
                beforeMethod = aspectMethodInfo.getMethod();
            }
            if (aspectMethodInfo.isAfter()) {
                afterMethod = aspectMethodInfo.getMethod();
            }
        }
        return new AopProxy(advisorInstance, beforeMethod, afterMethod);
    }

    public PointcutInfo findPointcutInfoByName(String name) {
        Iterator<PointcutInfo> iterator = iteratePointcutInfos();
        while (iterator.hasNext()) {
            PointcutInfo pointcutInfo = iterator.next();
            if (pointcutInfo != null && StringUtils.isEqual(pointcutInfo.getName(), name)) {
                return pointcutInfo;
            }
        }
        return null;
    }

    public void putPointcutAspect(PointcutInfo pointcutInfo, AspectMethodInfo aspectMethodInfo) {
        addPointcutIfNotExists(pointcutInfo);
        List<AspectMethodInfo> aspectMethodInfoList = getPointcutAspects(pointcutInfo);
        if (aspectMethodInfoList != null && aspectMethodInfo != null && !aspectMethodInfoList.contains(aspectMethodInfo)) {
            aspectMethodInfoList.add(aspectMethodInfo);
        }
    }

    public Set<PointcutInfo> pointcutInfos() {
        return _pointcuts.keySet();
    }

    public Iterator<PointcutInfo> iteratePointcutInfos() {
        return pointcutInfos().iterator();
    }

    /**
     * iterate advisor's pointcutInfos with outside definitions before
     * the outside pointcutInfo definitions may come from beans xml config
     *
     * @param outsidePointcutInfos
     * @return
     */
    public Iterator<PointcutInfo> iteratePointcutInfosWithOutsideDefinitions(PointcutInfo[] outsidePointcutInfos) {
        List<PointcutInfo> allPointcutInfos = new ArrayList<>();
        if (outsidePointcutInfos != null) {
            for (PointcutInfo pointcutInfo : outsidePointcutInfos) {
                allPointcutInfos.add(pointcutInfo);
            }
        }
        allPointcutInfos.addAll(pointcutInfos());
        return allPointcutInfos.iterator();
    }
}
