package zuice.aop;


import java.util.*;

import org.jsoup.nodes.Element;
import zuice.utils.Function;
import zuice.utils.ListUtils;
import zuice.utils.StringUtils;
import zuice.*;
import zuice.config.xml.*;
import zuice.exceptions.InjectRuntimeException;

/**
 * Created by zoowii on 14/11/4.
 */
public class AopConfigProcessor extends AbstractXmlEleProcessor {

    public AopConfigProcessor() {
        super();
        this.xmlns = XmlNamespaces.aop;
        this.label = "config";
    }

    private AopAdvisorInfo getInfoOfAdvisor(Class<?> cls) {
        return null; // TODO
    }

    @Override
    protected void process(Injector injector, Properties properties, Element ele) {
        final List<Element> advisorEles = ListUtils.filter(ele.children(), new Function<Element, Boolean>() {
            @Override
            public Boolean apply(Element node) {
                return "advisor".equals(getLabel(node));
            }
        });
        List<Element> pointcutEles = ListUtils.filter(ele.children(), new Function<Element, Boolean>() {
            @Override
            public Boolean apply(Element node) {
                return "pointcut".equals(getLabel(node));
            }
        });
        List<PointcutInfo> pointcutsFromXml = new ArrayList<>();
        for (Element pointcutEle : pointcutEles) {
            String pointcutId = null;
            if (!StringUtils.isEmpty(pointcutEle.attr("id"))) {
                pointcutId = pointcutEle.attr("id").trim();
            }
            String pointcutExpression = null;
            if (!StringUtils.isEmpty(pointcutEle.attr("expression"))) {
                pointcutExpression = pointcutEle.attr("expression").trim();
            }

            if (pointcutId != null && pointcutExpression != null)
                pointcutsFromXml.add(new PointcutInfo(pointcutId, pointcutExpression));
        }
        PointcutInfo[] pointcutsArrayFromXml = new PointcutInfo[pointcutsFromXml.size()];
        for (int i = 0; i < pointcutsFromXml.size(); ++i) {
            pointcutsArrayFromXml[i] = pointcutsFromXml.get(i);
        }
        for (Element advisorEle : advisorEles) {
            String adviceRef = null;
            if (!StringUtils.isEmpty(advisorEle.attr("advice-ref"))) {
                adviceRef = advisorEle.attr("advice-ref").trim();
            }
            if (adviceRef == null) {
                throw new InjectRuntimeException("aop advisor need ref to an Advisor bean");
            }
            // 产生一个BeanConstructor,使用这个构造器来产生bean
            BeanInfo beanInfo = injector.getBeanInfo(adviceRef);
            Object adviceInstance = injector.getBean(adviceRef);
            if (beanInfo == null || beanInfo.getComponentClass() == null) {
                throw new InjectRuntimeException("Can't find bean of " + adviceRef);
            }
            // 这里要找出advisor中pointcut关联的类和方法,在上面进行代理
            Class<?> advisorCls = beanInfo.getComponentClass();
            AopAdvisorInfo advisorInfo = AopFinder.findPointcutsInAdvice(advisorCls, pointcutsArrayFromXml);
            // 遍历pointcuts,找到起作用的bean class,然后修改它的beanConstructor
            Iterator<PointcutInfo> pointcutIterator = advisorInfo.iteratePointcutInfos();
            while (pointcutIterator.hasNext()) {
                PointcutInfo pointcutInfo = pointcutIterator.next();
                String[] prefixes = advisorInfo.getPointcutExecutionClassPrefixes(pointcutInfo);
                Iterator<BeanInfo> beanInfoIterator = injector.iterateBeanInfos();
                while (beanInfoIterator.hasNext()) {
                    BeanInfo clsBeanInfo = beanInfoIterator.next();
                    if (clsBeanInfo.getComponentClass() != null && StringUtils.startsWith(clsBeanInfo.getComponentClass().getCanonicalName(), prefixes)) {
                        AopProxy aopProxy = advisorInfo.createAopProxyFromPointcut(adviceInstance, pointcutInfo);
                        clsBeanInfo.setBeanConstructor(new ProxyBeanConstructor(clsBeanInfo.getComponentClass(), aopProxy, clsBeanInfo.getBeanConstructor()));
                    }
                }
            }
        }
    }
}
