package zuice.aop;

import zuice.aop.annotations.*;
import zuice.utils.StringUtils;

import java.lang.reflect.Method;

/**
 * AOP advice类中查找一些信息的辅助类
 * Created by zoowii on 14/12/17.
 */
public class AopFinder {
    /**
     * 从@Before("pointcut()")这类表达式的"pointcut()"中抽出pointcut点pointcut
     *
     * @param expr
     * @return
     */
    private static String getPointcutFromBeforeAfterExpr(String expr) {
        if (StringUtils.isEmpty(expr)) {
            return null;
        }
        String[] splited = expr.split("\\(\\)");
        if (splited.length > 0) {
            return splited[0];
        } else {
            return null;
        }
    }

    public static AopAdvisorInfo findPointcutsInAdvice(Class<?> adviceCls) {
        return findPointcutsInAdvice(adviceCls, null);
    }

    public static AopAdvisorInfo findPointcutsInAdvice(Class<?> adviceCls, PointcutInfo[] outsidePointcutInfos) {
        if (adviceCls == null) {
            return null;
        }
        AopAdvisorInfo aopAdvisorInfo = new AopAdvisorInfo();
        Method[] methods = adviceCls.getDeclaredMethods();
        // 首先找到所有的pointcuts
        for (Method method : methods) {
            Pointcut pointcutAnno = method.getAnnotation(Pointcut.class);
            if (pointcutAnno != null && !StringUtils.isEmpty(pointcutAnno.value())) {
                PointcutInfo pointcutInfo = new PointcutInfo(method.getName(), pointcutAnno.value().trim());
                aopAdvisorInfo.addPointcutIfNotExists(pointcutInfo);
            }
        }
        if (outsidePointcutInfos != null) {
            for (PointcutInfo pointcutInfo : outsidePointcutInfos) {
                aopAdvisorInfo.addPointcutIfNotExists(pointcutInfo);
            }
        }
        // 找到所有@Before和@After注解的方法
        for (Method method : methods) {
            Before beforeAnno = method.getAnnotation(Before.class);
            After afterAnno = method.getAnnotation(After.class);
            if (beforeAnno != null && !StringUtils.isEmpty(beforeAnno.value())) {
                String beforeAnnoExpr = beforeAnno.value().trim();
                String pointcutName = getPointcutFromBeforeAfterExpr(beforeAnnoExpr);
                if (!StringUtils.isEmpty(pointcutName)) {
                    PointcutInfo pointcutInfo = aopAdvisorInfo.findPointcutInfoByName(pointcutName);
                    if (pointcutInfo != null) {
                        AspectMethodInfo aspectMethodInfo = new AspectMethodInfo(true, method);
                        aopAdvisorInfo.putPointcutAspect(pointcutInfo, aspectMethodInfo);
                    }
                }
            }
            if (afterAnno != null && !StringUtils.isEmpty(afterAnno.value())) {
                String afterAnnoExpr = afterAnno.value().trim();
                String pointcutName = getPointcutFromBeforeAfterExpr(afterAnnoExpr);
                if (!StringUtils.isEmpty(pointcutName)) {
                    PointcutInfo pointcutInfo = aopAdvisorInfo.findPointcutInfoByName(pointcutName);
                    if (pointcutInfo != null) {
                        AspectMethodInfo aspectMethodInfo = new AspectMethodInfo(false, method);
                        aopAdvisorInfo.putPointcutAspect(pointcutInfo, aspectMethodInfo);
                    }
                }
            }
        }
        return aopAdvisorInfo;
    }
}
