package zuice.aop;

import java.lang.reflect.Method;

/**
 * 切入的方法的信息
 */
public class AspectMethodInfo {
    private boolean isBefore;
    private Method method;

    /**
     * @param isBefore 如果是true表示before方法,否则就是after方法
     * @param method
     */
    public AspectMethodInfo(boolean isBefore, Method method) {
        this.isBefore = isBefore;
        this.method = method;
    }

    public boolean isBefore() {
        return isBefore;
    }

    public void setBefore(boolean isBefore) {
        this.isBefore = isBefore;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public boolean isAfter() {
        return !isBefore();
    }
}
