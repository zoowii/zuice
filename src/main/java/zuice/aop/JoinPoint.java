package zuice.aop;

import java.lang.reflect.Method;

/**
 * Created by zoowii on 14/11/4.
 */
public interface JoinPoint {
    public Object getOriginInstance();

    public Method getOriginMethod();

    public Object[] getOriginArgs();
}
