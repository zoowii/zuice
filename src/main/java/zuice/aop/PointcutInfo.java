package zuice.aop;

/**
 * 记录aop pointcup的信息
 */
public class PointcutInfo {
    private String name;
    private String expression;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    /**
     * @param name       pointcup的名称
     * @param expression pointcup作用位置的表达式
     */
    public PointcutInfo(String name, String expression) {
        this.name = name;
        this.expression = expression;
    }
}
