package zuice.aop;

import java.lang.reflect.Method;

/**
 * Created by zoowii on 14/12/17.
 */
public class SimpleJointPoint implements JoinPoint {
    private Object instance;
    private Method method;
    private Object[] args;

    @Override
    public Object getOriginInstance() {
        return instance;
    }

    @Override
    public Method getOriginMethod() {
        return method;
    }

    @Override
    public Object[] getOriginArgs() {
        return args;
    }

    public Object getInstance() {
        return instance;
    }

    public void setInstance(Object instance) {
        this.instance = instance;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public SimpleJointPoint(Object instance, Method method, Object[] args) {
        this.instance = instance;
        this.method = method;
        this.args = args;
    }
}
