package zuice.aop.annotations;

/**
 * Created by zoowii on 14/11/4.
 */
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target({java.lang.annotation.ElementType.TYPE})
public @interface Aspect {
    java.lang.String value() default "";
}
