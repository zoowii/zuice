package zuice.config.xml;

import java.util.Properties;

import org.jsoup.nodes.Element;
import zuice.Injector;
import zuice.utils.StringUtils;

/**
 * Created by zoowii on 14/11/1.
 */
public abstract class AbstractXmlEleProcessor {
    protected String xmlns;
    protected String label;
    protected XmlNamespaceResolver currentXmlnsResolver;

    protected abstract void process(Injector injector, Properties properties, Element ele);

    protected String getLabel(Element ele) {
        XmlNamespaceResolver.ScopeLabel scopeLabel = currentXmlnsResolver.getScope(ele.tagName());
        return scopeLabel.getLabel();
    }

    public void checkAndProcessXmlElement(Injector injector, Properties properties, XmlNamespaceResolver xmlnsResolver, Element ele) {
        this.currentXmlnsResolver = xmlnsResolver;
        XmlNamespaceResolver.ScopeLabel scopeLabel = xmlnsResolver.getScope(ele.tagName());
        String prefix = scopeLabel.getPrefix();
        String xmlns2 = xmlnsResolver.getScopeUri(prefix);
        String label2 = scopeLabel.getLabel();
        if (StringUtils.isEqual(this.xmlns, xmlns2) && StringUtils.isEqual(this.label, label2)) {
            process(injector, properties, ele);
        }
    }

}
