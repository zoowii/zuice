package zuice.config.xml;

import java.io.*;
import java.util.*;
import java.util.Properties;

import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.jsoup.parser.Parser;
import zuice.Injector;
import zuice.aop.AopConfigProcessor;
import zuice.config.xml.elementprocessors.context.*;
import zuice.config.xml.elementprocessors.*;

import zuice.exceptions.InjectRuntimeException;

/**
 * Created by zoowii on 14/10/31.
 */
public class ClassPathXmlConfigLoader {
    public static final List<AbstractXmlEleProcessor> xmlEleProcessors = new ArrayList<>();

    public static void registerXmlEleProcessor(AbstractXmlEleProcessor processor) {
        xmlEleProcessors.add(processor);
    }

    public static void deRegisterAllXmlEleProcessors() {
        synchronized (xmlEleProcessors) {
            xmlEleProcessors.clear();
            initialize();
        }
    }

    public static void initialize() {
        if (xmlEleProcessors.isEmpty()) {
            registerXmlEleProcessor(new ImportProcessor());
            registerXmlEleProcessor(new BeanProcessor());
            registerXmlEleProcessor(new ComponentClassProcessor());
            registerXmlEleProcessor(new ComponentScanProcessor());
            registerXmlEleProcessor(new PropertyPlaceHolderProcessor());
            registerXmlEleProcessor(new AopConfigProcessor());
        }
    }

    public static Iterator<AbstractXmlEleProcessor> iterateXmlEleProcessors() {
        return xmlEleProcessors.iterator();
    }

    public static Injector createInjector(String xmlPath) {
        initialize();
        Injector injector = Injector.newInstance();
        Properties properties = new Properties();
        loadXmlConfigToInjector(injector, properties, xmlPath);
        return injector;
    }

    public static void loadXmlConfigToInjector(Injector injector, Properties properties, String xmlPath) {
        InputStream xmlInputStream;
        try {
            xmlInputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(xmlPath);
        } catch (Exception e) {
            xmlInputStream=null;
        }
        if(xmlInputStream==null){
            try {
                xmlInputStream = new FileInputStream(new File(xmlPath));
            }catch (IOException e){
                xmlInputStream = null;
            }
        }
        if(xmlInputStream==null) {
            throw new RuntimeException(String.format("Can't load config xml in path: %s", xmlPath));
        }
        try {
            Element beansEle = Jsoup.parse(xmlInputStream, "UTF-8", "", Parser.xmlParser()).select("beans").first();
            XmlNamespaceResolver xmlnsResolver = new XmlNamespaceResolver(beansEle);
            for (Element ele : beansEle.children()) {
                Iterator<AbstractXmlEleProcessor> iterator = iterateXmlEleProcessors();
                while (iterator.hasNext()) {
                    AbstractXmlEleProcessor processor = iterator.next();
                    processor.checkAndProcessXmlElement(injector, properties, xmlnsResolver, ele);
                }
            }
            injector.callPendingActions();
        } catch (IOException e) {
            throw new InjectRuntimeException(e);
        }
    }
}
