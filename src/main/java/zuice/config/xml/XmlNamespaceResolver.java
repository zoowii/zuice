package zuice.config.xml;

import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by zoowii on 14/11/4.
 */
public class XmlNamespaceResolver {
    public static final class ScopeLabel {
        private String scope;
        private String prefix;
        private String label;

        public String getScope() {
            return scope;
        }

        public void setScope(String scope) {
            this.scope = scope;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getPrefix() {
            return prefix;
        }

        public void setPrefix(String prefix) {
            this.prefix = prefix;
        }
    }

    private static final String XMLNS = "xmlns";
    private final Map<String, String> scopes = new HashMap<>();

    public XmlNamespaceResolver(Element ele) {
        Attributes attrs = ele.attributes();
        for (Attribute attribute : attrs) {
            String key = attribute.getKey();
            if (key.startsWith(XMLNS + ":") || key.equals(XMLNS)) {
                String scopeName = key.substring(XMLNS.length());
                if (scopeName.startsWith(":")) {
                    scopeName = scopeName.substring(1);
                }
                scopes.put(scopeName, attribute.getValue());
            }
        }
    }

    public ScopeLabel getScope(String tagFullName) {
        String[] splited = tagFullName.split(":");
        String scopeName;
        String label;
        if (splited.length < 2) {
            scopeName = "";
            label = tagFullName;
        } else {
            scopeName = splited[0];
            label = splited[1];
        }
        ScopeLabel scopeLabel = new ScopeLabel();
        scopeLabel.setLabel(label);
        scopeLabel.setScope(getScopeUri(scopeName));
        scopeLabel.setPrefix(scopeName);
        return scopeLabel;
    }

    public String getScopeUri(String prefix) {
        return scopes.get(prefix);
    }
}
