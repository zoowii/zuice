package zuice.config.xml;

/**
 * Created by zoowii on 14/11/4.
 */
public class XmlNamespaces {
    public static final String beans = "http://www.zuice.org/schema/beans";
    public static final String context = "http://www.zuice.org/schema/context";
    public static final String aop = "http://www.zuice.org/schema/aop";
}
