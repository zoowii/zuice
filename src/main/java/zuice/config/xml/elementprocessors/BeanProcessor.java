package zuice.config.xml.elementprocessors;


import java.io.IOException;
import java.util.*;

import org.jsoup.nodes.Element;
import zuice.exceptions.InjectRuntimeException;
import zuice.utils.*;
import zuice.*;
import zuice.config.xml.*;

/**
 * Created by zoowii on 14/11/1.
 */
public class BeanProcessor extends AbstractXmlEleProcessor {
    public BeanProcessor() {
        super();
        this.xmlns = XmlNamespaces.beans;
        this.label = "bean";
    }

    @Override
    protected void process(Injector injector, final Properties properties, Element ele) {
        String cls = null;
        if (!StringUtils.isEmpty(ele.attr("class"))) {
            cls = ele.attr("class").trim();
        }
        String id = null;
        if (!StringUtils.isEmpty(ele.attr("id"))) {
            id = ele.attr("id").trim();
        }
        String name = null;
        if (!StringUtils.isEmpty(ele.attr("name"))) {
            name = ele.attr("name").trim();
        }
        String scope = null;
        if (!StringUtils.isEmpty(ele.attr("scope"))) {
            scope = ele.attr("scope").trim();
        }
        boolean isLazy = true;
        if (!StringUtils.isEmpty(ele.attr("lazy-init"))) {
            isLazy = Boolean.valueOf(ele.attr("lazy-init").trim());
        }
        String initMethod = null;
        if (!StringUtils.isEmpty(ele.attr("init-method"))) {
            initMethod = ele.attr("init-method").trim();
        }
        String destroyMethod = null;
        if (!StringUtils.isEmpty(ele.attr("destroy-method"))) {
            destroyMethod = ele.attr("destroy-method").trim();
        }
        String beanName;
        if (id != null) {
            beanName = id;
        } else {
            beanName = name;
        }
        // 读取constructor-args和properties
        List<Element> constructorArgEles = ListUtils.filter(ele.children(), new Function<Element, Boolean>() {
            @Override
            public Boolean apply(Element node) {
                return "constructor-arg".equals(getLabel(node));
            }
        });
        List<BeanInfoValue> constructorArgs = ListUtils.map(constructorArgEles, new Function<Element, BeanInfoValue>() {
            @Override
            public BeanInfoValue apply(Element node) {
                Element nodeEle = node;
                String ref = null;
                String value = null;
                if (!StringUtils.isEmpty(nodeEle.attr("ref"))) {
                    ref = nodeEle.attr("ref").trim();
                }
                if (!StringUtils.isEmpty(nodeEle.attr("value"))) {
                    value = nodeEle.attr("value").trim();
                }
                if (value != null) {
                    try {
                        value = TemplateUtil.render(value, properties);
                    } catch (IOException e) {
                        value = null;
                    }
                }
                if (ref != null) {
                    return new BeanInfoValue.RefBeanInfoValue(ref);
                } else {
                    return new BeanInfoValue.PrimitiveBeanInfoValue(value);
                }
            }
        });

        final Map<String, BeanInfoValue> props = new HashMap<>();
        ListUtils.map(ListUtils.filter(ele.children(), new Function<Element, Boolean>() {
            @Override
            public Boolean apply(Element node) {
                return "property".equals(getLabel(node));
            }
        }), new Function<Element, Object>() {
            @Override
            public Object apply(Element node) {
                Element nodeEle = node;
                String name = null;
                if (!StringUtils.isEmpty(nodeEle.attr("name"))) {
                    name = nodeEle.attr("name").trim();
                }
                if (name == null) {
                    throw new InjectRuntimeException("property name can't be null");
                }
                String ref = null;
                String value = null;
                if (!StringUtils.isEmpty(nodeEle.attr("ref"))) {
                    ref = nodeEle.attr("ref").trim();
                }
                if (!StringUtils.isEmpty(nodeEle.attr("value"))) {
                    value = nodeEle.attr("value").trim();
                }
                if (value != null) {
                    try {
                        value = TemplateUtil.render(value, properties);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
                BeanInfoValue beanInfoValue;
                if (ref != null) {
                    beanInfoValue = new BeanInfoValue.RefBeanInfoValue(ref);
                } else {
                    beanInfoValue = new BeanInfoValue.PrimitiveBeanInfoValue(value);
                }
                props.put(name, beanInfoValue);
                return null;
            }
        });

        BeanInfo beanInfo = new BeanInfo(constructorArgs, props);
        beanInfo.setLazyBean(isLazy);
        beanInfo.setInitMethod(initMethod);
        beanInfo.setDestroyMethod(destroyMethod);
        if (BeanScope.SINGLETON.equals(scope)) {
            beanInfo.setSingleton(true);
        } else if (BeanScope.INJECT.equals(scope)) {
            beanInfo.setInjectScope(true);
        } else if (BeanScope.THREAD.equals(scope)) {
            beanInfo.setThreadScope(true);
        }
        try {
            if (beanName != null) {
                injector.addBeanBind(beanName, Class.forName(cls), beanInfo, true);
            } else {
                injector.addBeanBind(Class.forName(cls), beanInfo, true);
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
