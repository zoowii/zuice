package zuice.config.xml.elementprocessors;

import java.util.Properties;

import org.jsoup.nodes.Element;
import zuice.Injector;
import zuice.config.xml.*;

import zuice.utils.StringUtils;

/**
 * Created by zoowii on 2015/1/16.
 */
public class ComponentClassProcessor extends AbstractXmlEleProcessor {
    public ComponentClassProcessor() {
        super();
        this.xmlns = XmlNamespaces.beans;
        this.label = "component-class";
    }

    @Override
    protected void process(Injector injector, Properties properties, Element ele) {
        if (!StringUtils.isEmpty(ele.attr("type"))) {
            String typeClassName = ele.attr("type").trim();
            try {
                injector.addComponentAnnotation(Class.forName(typeClassName));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
