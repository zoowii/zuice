package zuice.config.xml.elementprocessors;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;
import java.util.regex.Pattern;

import org.jsoup.nodes.Element;
import zuice.Injector;
import zuice.config.xml.*;
import zuice.utils.StringUtils;

/**
 * Created by zoowii on 14/11/1.
 */
public class ImportProcessor extends AbstractXmlEleProcessor {
    public ImportProcessor() {
        super();
        this.xmlns = XmlNamespaces.beans;
        this.label = "import";
    }

    private String determineRootDir(String location) {
        int prefixEnd = location.indexOf(":") + 1;
        int rootDirEnd = location.length();
        if (rootDirEnd > prefixEnd && (location.substring(prefixEnd, rootDirEnd).contains("/"))) {
            rootDirEnd = location.lastIndexOf('/', rootDirEnd - 2) + 1;
        } else {
            return "";
        }
        if (rootDirEnd == 0) {
            rootDirEnd = prefixEnd;
        }
        return location.substring(0, rootDirEnd);
    }
    
    private boolean matchFileNamePattern(String fileName, String pattern){
        return fileName.equals(pattern) || fileName.matches(pattern.replace("*", ".*"));
    }

    private static final String CLASSPATH_URL_PREFIX = "classpath:";
    private static final String CLASSPATH_ALL_URL_PREFIX = "classpath*:";
    private static final String FILE_URL_PREFIX = "file:";

    /**
     * * 模糊匹配考虑*字符,*可能是最开头,也可能是中间,如果是在中间,则可能是path/abc*def/remaining
     * @param injector
     * @param properties
     * @param ele
     */
    @Override
    protected void process(Injector injector, Properties properties, Element ele) {
        if (!StringUtils.isEmpty(ele.attr("resource"))) {
            String resourcePath = ele.attr("resource").trim();
            // now just support classpath: , classpath*: and file:
            String fullResourcePath = resourcePath;
            if(resourcePath.startsWith(CLASSPATH_URL_PREFIX)) {
                resourcePath = resourcePath.substring(CLASSPATH_URL_PREFIX.length());
            } else if(resourcePath.startsWith(CLASSPATH_ALL_URL_PREFIX)) {
                resourcePath = resourcePath.substring(CLASSPATH_ALL_URL_PREFIX.length());
            } else if(resourcePath.startsWith(FILE_URL_PREFIX)) {
                resourcePath = resourcePath.substring(FILE_URL_PREFIX.length());
            }
            String rootDir = determineRootDir(resourcePath);
            String fileNamePattern = resourcePath.substring(rootDir.length());
            if(rootDir.isEmpty()){
                rootDir = ".";
            }
            try {
                Enumeration<URL> rootDirResourceUrls = this.getClass().getClassLoader().getResources(rootDir);
                while(rootDirResourceUrls.hasMoreElements()){
                    URL rootDirResourceUrl = rootDirResourceUrls.nextElement();
                    if (rootDirResourceUrl == null) {
                        continue;
                    }
                    String rootDirPath = rootDirResourceUrl.getFile();
                    File rootDirFile = new File(rootDirPath);
                    File[] subFiles = rootDirFile.listFiles();
                    if (subFiles == null) {
                        continue;
                    }
                    for (File file : subFiles) {
                        if (matchFileNamePattern(file.getName(), fileNamePattern)) {
                            ClassPathXmlConfigLoader.loadXmlConfigToInjector(injector, properties, file.getAbsolutePath());
                        }
                    }
                    if(fullResourcePath.startsWith(CLASSPATH_ALL_URL_PREFIX)) {
                        break;
                    }
                }
            }catch (IOException e){
                throw new RuntimeException(e);
            }
        }
    }
}
