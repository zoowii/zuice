package zuice.config.xml.elementprocessors.context;

import java.util.Properties;

import org.jsoup.nodes.Element;
import zuice.Injector;
import zuice.config.xml.*;
import zuice.utils.StringUtils;

/**
 * Created by zoowii on 14/11/1.
 */
public class ComponentScanProcessor extends AbstractXmlEleProcessor {
    public ComponentScanProcessor() {
        super();
        this.xmlns = XmlNamespaces.context;
        this.label = "component-scan";
    }

    @Override
    protected void process(Injector injector, Properties properties, Element ele) {
        if (!StringUtils.isEmpty(ele.attr("base-package"))) {
            injector.scanPackage(ele.attr("base-package").trim());
        }
    }
}
