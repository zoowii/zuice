package zuice.config.xml.elementprocessors.context;

import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.jsoup.nodes.Element;
import zuice.Injector;
import zuice.config.xml.*;
import zuice.utils.IOUtils;
import zuice.utils.ListUtils;
import zuice.utils.StringUtils;

/**
 * Created by zoowii on 14/11/1.
 */
public class PropertyPlaceHolderProcessor extends AbstractXmlEleProcessor {
    public PropertyPlaceHolderProcessor() {
        super();
        this.xmlns = XmlNamespaces.context;
        this.label = "property-placeholder";
    }

    @Override
    protected void process(Injector injector, Properties properties, Element ele) {
        if (!StringUtils.isEmpty(ele.attr("location"))) {
            String location = ele.attr("location").trim();
            InputStream stream;
            Properties newProperties = new Properties();
            if (location.startsWith("classpath:")) {
                String classPathLocation = location.substring("classpath:".length());
                stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(classPathLocation);
            } else {
                try {
                    stream = new FileInputStream(new java.io.File(location));
                } catch (FileNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
            try {
                newProperties.load(stream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                IOUtils.closeQuietly(stream);
            }
            for (Object o : newProperties.keySet()) {
                String key = (String) o;
                properties.put(key, newProperties.get(key));
            }
        }
    }
}
