package zuice.exceptions;

/**
 * Created by zoowii on 14/11/1.
 */
public class InjectRuntimeException extends RuntimeException {
    public InjectRuntimeException() {
        super();
    }

    public InjectRuntimeException(String msg) {
        super(msg);
    }

    public InjectRuntimeException(Exception e) {
        super(e);
    }
}
