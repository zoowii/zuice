package zuice.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by zoowii on 14/10/29.
 */
public class BeanUtils {
    public static Constructor<?> getConstructorWithNParameters(Class<?> cls, int n) {
        Constructor<?>[] constructors = cls.getConstructors();
        for (Constructor<?> constructor : constructors) {
            if (constructor.getParameterTypes().length == n) {
                return constructor;
            }
        }
        return null;
    }

    /**
     * 获取注解的某个属性的值
     *
     * @param annotation
     * @param property
     * @return
     */
    public static Object getPropertyOfAnnotation(Annotation annotation, String property) {
        Class<?> cls = annotation.getClass();
        Method method;
        try {
            method = cls.getMethod(property);
        } catch (NoSuchMethodException e) {
            return null;
        }
        if (method == null) {
            return null;
        }
        try {
            return method.invoke(annotation);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
}
