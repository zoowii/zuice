package zuice.utils;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import zuice.ProxyAdvised;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by zoowii on 14/10/29.
 */
public class ClassUtils {
    private static final Logger LOG = Logger.getLogger(ClassUtils.class);

    public static Method getMethodByName(Class<?> cls, String name) {
        if (cls == null || StringUtils.isEmpty(name)) {
            return null;
        }
        for (Method method : cls.getDeclaredMethods()) {
            if (method.getName().equals(name)) {
                return method;
            }
        }
        return null;
    }

    public static Class<?> getRealClassOverProxyAdvised(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof ProxyAdvised) {
            ProxyAdvised proxyAdvised = (ProxyAdvised) obj;
            if (isAssignableFrom(ProxyAdvised.class, proxyAdvised.getTargetClass())) {
                return getRealClassOverProxyAdvised(proxyAdvised.getProxyInstance());
            } else {
                return proxyAdvised.getTargetClass();
            }
        } else {
            return obj.getClass();
        }
    }

    public static Object callConstructor(Constructor<?> constructor, Object[] params) throws Exception {
        return constructor.newInstance(params);
    }

    public static Object callMethod(Method method, Object instance, Object[] args) throws InvocationTargetException, IllegalAccessException {
        if (method == null || instance == null) {
            return null;
        }
        if (args == null) {
            args = new Object[0];
        }
        if (Proxy.isProxyClass(instance.getClass())) {
            try {
                return Proxy.getInvocationHandler(instance).invoke(instance, method, args);
            } catch (Throwable throwable) {
                if (throwable.getCause() instanceof RuntimeException) {
                    throw (RuntimeException) throwable.getCause();
                }
                throw new InvocationTargetException(throwable);
            }
        } else {
            return method.invoke(instance, args);
        }
    }

    public static boolean isAssignableFrom(Class<?> declareCls, Class<?> implCls) {
        return declareCls != null && implCls != null && (declareCls == implCls || declareCls.isAssignableFrom(implCls));
    }

    public static Annotation getAnnotationOfClass(Class<?> cls, Class annotationCls) {
        return cls.getAnnotation(annotationCls);
    }
    
    public static List<String> getClassNamesInJar(String jarPath){
        List<String> result = new ArrayList<>();
        try {
            JarFile jarFile = new JarFile(jarPath);
            Enumeration<JarEntry> entries = jarFile.entries();
            while(entries.hasMoreElements()){
                JarEntry jarEntry = entries.nextElement();
                String name = jarEntry.getName();
                if(name.endsWith(".class")) {
                    name = name.substring(0, name.length()-".class".length());
                    name = name.replaceAll("/", ".");
                    result.add(name);
                }
            }
        }catch (IOException e){
            LOG.error(e);
        }
        return result;
    }
    
    public static List<File> listFiles(File directory){
        List<File> files = new ArrayList<>();
        if(directory==null||!directory.exists() || !directory.isDirectory()) {
            return files;
        }
        File[] subFiles = directory.listFiles();
        if(subFiles==null){
            return files;
        }
        for(File file:subFiles){
            if(file.isDirectory()){
                files.addAll(listFiles(file));
            } else {
                files.add(file);
            }
        }
        return files;
    }
    
    public static List<String> getClassNamesInDirectory(String dir){
        dir = new File(dir).getAbsolutePath();
        List<String> result = new ArrayList<>();
        File dirFile = new File(dir);
        if(!dirFile.exists()) {
            return result;
        }
        List<File> subFiles = listFiles(dirFile);
        for(File file : subFiles){
            if(file.isDirectory() || !file.isFile()){
                continue;
            }
            String name = file.getAbsolutePath();
            if(name.endsWith(".class") && name.startsWith(dir)) {
                name = name.substring(0, name.length()-".class".length());
                name = name.substring(dir.length());
                name = name.replaceAll("[/\\\\]", ".");
                if(name.startsWith(".")) {
                    name = name.substring(1);
                }
                result.add(name);
            }
        }
        return result;
    }

    public static ClassLoader contextClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    public static ClassLoader staticClassLoader() {
        return ClassUtils.class.getClassLoader();
    }

    public static ClassLoader[] classLoaders(ClassLoader... classLoaders) {
        if(classLoaders != null && classLoaders.length != 0) {
            return classLoaders;
        } else {
            ClassLoader contextClassLoader = contextClassLoader();
            ClassLoader staticClassLoader = staticClassLoader();
            return contextClassLoader != null?(staticClassLoader != null && contextClassLoader != staticClassLoader?new ClassLoader[]{contextClassLoader, staticClassLoader}:new ClassLoader[]{contextClassLoader}):new ClassLoader[0];
        }
    }

    private static Collection<URL> distinctUrls(Collection<URL> urls) {
        try {
            HashSet<URI> e = new HashSet<>(urls.size());

            for (Object i$ : urls) {
                e.add(((URL) i$).toURI());
            }

            ArrayList<URL> result1 = new ArrayList<>(e.size());

            for (Object anE : e) {
                URI uri = (URI) anE;
                result1.add(uri.toURL());
            }

            return result1;
        } catch (Exception var5) {
            Set<URL> result = new HashSet<>();
            result.addAll(urls);
            return result;
        }
    }
    
    public static Collection<URL> forClassLoader(ClassLoader... classLoaders) {
        ArrayList<URL> result = new ArrayList<>();
        ClassLoader[] loaders = classLoaders(classLoaders);
        ClassLoader[] arr$ = loaders;
        int len$ = loaders.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            for(ClassLoader classLoader = arr$[i$]; classLoader != null; classLoader = classLoader.getParent()) {
                if(classLoader instanceof URLClassLoader) {
                    URL[] urls = ((URLClassLoader)classLoader).getURLs();
                    result.addAll(ListUtils.newHashSet(urls));
                }
            }
        }

        return distinctUrls(result);
    }
    
    public static List<String> getAllClassNames(){
        List<ClassLoader> classLoadersList = new ArrayList<>();
        classLoadersList.add(contextClassLoader());
        classLoadersList.add(staticClassLoader());
        Collection<URL> urls = forClassLoader(classLoadersList.toArray(new ClassLoader[2]));
        Iterator<URL> urlIterator = urls.iterator();
        List<String> classNames = new ArrayList<>();
        while(urlIterator.hasNext()){
            URL url = urlIterator.next();
            String path = url.getPath();
            if(path.endsWith(".jar") || path.endsWith(".war")) {
                classNames.addAll(getClassNamesInJar(path));
            } else {
                classNames.addAll(getClassNamesInDirectory(path));
            }
        }
        return classNames;
    }
    
    public static List<Class<?>> findClassesInPackage(String packagePath){
        List<Class<?>> result = new ArrayList<>();
        try {
            for(String className : getAllClassNames()) {
                if(className.startsWith(packagePath)) {
                    try {
                        result.add(Class.forName(className));
                    } catch (Exception e){
                        LOG.error(e);
                    }
                }
            }
            return result;
        }catch (Exception e){
            return result;
        }
    }

    /**
     * 获取到带有annotationClasses中任何一个注解的类型列表
     *
     * @param classes
     * @param annotationClasses
     * @return Pair[类型, 注解]
     */
    @SuppressWarnings("all")
    public static List<Pair<Class<?>, Annotation>> filterClassWithAnyAnnotationsInList(List<Class<?>> classes, List<Class<?>> annotationClasses) {
        List<Pair<Class<?>, Annotation>> result = new ArrayList<>();
        if (classes == null || annotationClasses == null) {
            return result;
        }
        for (Class<?> cls : classes) {
            for (Class<?> annotationCls : annotationClasses) {
                Annotation annotation = cls.getAnnotation((Class<? extends Annotation>) annotationCls);
                if (annotation != null) {
                    result.add((Pair<Class<?>, Annotation>) Pair.of(cls, annotation));
                    break;
                }
            }
        }
        return result;
    }

    public static Method getDeclaredMethod(Class<?> cls, String methodName) {
        try {
            return cls.getDeclaredMethod(methodName);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    public static Object invokeMethod(Method method, Object instance, Object... args) {
        try {
            return callMethod(method, instance, args);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public static Object parseToPrimitiveTypeValue(Class<?> cls, Object value) {
        if (value == null || cls == null) {
            return value;
        }
        if (cls == Integer.class || cls.getName().equals("int")) {
            return Integer.valueOf(value.toString());
        } else if (cls == Long.class || cls.getName().equals("long")) {
            return Long.valueOf(value.toString());
        } else if (cls == Boolean.class || cls.getName().equals("boolean")) {
            return Boolean.valueOf(value.toString());
        } else if (cls == Float.class || cls.getName().equals("float")) {
            return Float.valueOf(value.toString());
        } else if (cls == Double.class || cls.getName().equals("double")) {
            return Double.valueOf(value.toString());
        } else if (cls == Date.class) {
            if (value.getClass() == Integer.class || value.getClass() == Long.class || value.toString().matches("^\\d+$")) {
                return new Date(Long.valueOf(value.toString()));
            } else {
                try {
                    return DateUtils.parseDate(value.toString(),
                            "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd",
                            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss",
                            "yyyy-MM-dd'T'HH:mm:ss.SSSz", "yyyy-MM-dd'T'HH:mm:ss",
                            "yyyy-MM-dd'T'HH:mm:ss.SSSxxx");
                } catch (ParseException e) {
                    return value;
                }
            }
        } else if (cls == Timestamp.class) {
            return new Timestamp(((Date) parseToPrimitiveTypeValue(Date.class, value)).getTime());
        } else {
            return value;
        }
    }
}
