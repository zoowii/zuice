package zuice.utils;

/**
 * Created by zoowii on 14/11/1.
 */
public interface Function<T, R> {
    public R apply(T param);
}
