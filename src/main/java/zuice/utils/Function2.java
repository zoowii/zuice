package zuice.utils;

/**
 * Created by zoowii on 2015/2/3.
 */
public interface Function2<T1, T2, R> {
    public R apply(T1 param1, T2 param2);
}
