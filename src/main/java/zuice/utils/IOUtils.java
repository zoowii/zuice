package zuice.utils;

import java.io.Closeable;
import java.io.IOException;

/**
 * Created by zoowii on 2015/3/3.
 */
public class IOUtils {
    public static void closeQuietly(Closeable stream){
        if(stream!=null) {
            try {
                stream.close();
            } catch (IOException e) {

            }
        }
    }
}
