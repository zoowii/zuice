package zuice.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by zoowii on 14/10/31.
 */
public class ListUtils {
    public static <T> boolean inArray(Object item, T[] col) {
        if (col == null) {
            return false;
        }
        for (T listItem : col) {
            if (listItem == item) {
                return true;
            }
        }
        return false;
    }

    public static Annotation firstInAnnotationArrayOfClass(Annotation[] col, Class<?> cls) {
        if (col == null || cls == null) {
            return null;
        }
        for (Annotation annotation : col) {
            if (cls.isInstance(annotation)) {
                return annotation;
            }
        }
        return null;
    }

    public static <T1, T2> List<T2> map(List<T1> source, Function<T1, T2> fn) {
        if (source == null || fn == null) {
            return null;
        }
        List<T2> result = new ArrayList<>();
        for (T1 item : source) {
            result.add(fn.apply(item));
        }
        return result;
    }

    public static <T> List<T> filter(List<T> source, Function<T, Boolean> fn) {
        if (source == null || fn == null) {
            return source;
        }
        List<T> result = new ArrayList<>();
        for (T item : source) {
            Boolean testFn = fn.apply(item);
            if (testFn != null && testFn) {
                result.add(item);
            }
        }
        return result;
    }

    @SuppressWarnings("all")
    public static <T> T[] asArray(List<T> col, Class<T> type) {
        if (col == null) {
            return null;
        }
        T[] array = (T[]) Array.newInstance(type, col.size());
        col.toArray(array);
        return array;
    }

    public static <T> List<T> concat(T[]... arrays) {
        List<T> result = new ArrayList<>();
        for (int i = 0; i < arrays.length; ++i) {
            T[] array = arrays[i];
            if (array != null) {
                for (T item : array) {
                    result.add(item);
                }
            }
        }
        return result;
    }

    public static Object firstNotNull(Object... values) {
        for (Object value : values) {
            if (value != null)
                return value;
        }
        return null;
    }
    public static <T> Set<T> newHashSet(Collection<T> source){
        Set<T> result = new HashSet<>();
        if(source!=null) {
            result.addAll(source);
        }
        return result;
    }
    public static <T> Set<T> newHashSet(T[] source){
        Set<T> result = new HashSet<>();
        if(source!=null) {
            for(T item: source){
                result.add(item);
            }
        }
        return result;
    }
}