package zuice.utils;

import java.util.*;

/**
 * 这是一个FirstKey => SecondKey => Value的数据结构,可以从某个Key获取和设置其他内容的值
 * Created by zoowii on 14/11/5.
 */
public class MultiHashMap<FK, SK, V extends Object> {
    private Map<FK, V> fkvMap = new HashMap<>();
    private Map<SK, V> skvMap = new HashMap<>();
    private Map<FK, SK> fkskMap = new HashMap<>();
    private Map<SK, FK> skfkMap = new HashMap<>();

    public synchronized SK getSecondKeyByFirstKey(FK fk) {
        return fkskMap.get(fk);
    }


    public synchronized FK getFirstKeyBySecondKey(SK sk) {
        return skfkMap.get(sk);
    }

    public synchronized V getByFirstKey(FK fk) {
        return (V) ListUtils.firstNotNull(fkvMap.get(fk), skvMap.get(getSecondKeyByFirstKey(fk)));
    }

    public synchronized V getBySecondKey(SK sk) {
        return (V) ListUtils.firstNotNull(skvMap.get(sk), fkvMap.get(getFirstKeyBySecondKey(sk)));
    }

    public boolean containsFirstKey(FK fk) {
        return getByFirstKey(fk) != null;
    }

    public boolean containsSecondKey(SK sk) {
        return getBySecondKey(sk) != null;
    }

    public void putSecondKeyByFirstKey(FK fk, SK sk) {
        if (fk != null && getByFirstKey(fk) != null) {
            synchronized (this) {
                SK oldSk = getSecondKeyByFirstKey(fk);
                if (oldSk != null) {
                    fkskMap.remove(fk);
                    skfkMap.remove(oldSk);
                    skvMap.remove(oldSk);
                    if (sk != null) {
                        fkskMap.put(fk, sk);
                        skfkMap.put(sk, fk);
                        skvMap.put(sk, getByFirstKey(fk));
                    }
                }
            }
        }
    }

    public void putFirstKeyBySecondKey(SK sk, FK fk) {
        if (sk != null && getBySecondKey(sk) != null) {
            synchronized (this) {
                FK oldFk = getFirstKeyBySecondKey(sk);
                if (oldFk != null) {
                    skfkMap.remove(sk);
                    fkskMap.remove(oldFk);
                    fkvMap.remove(oldFk);
                    if (fk != null) {
                        skfkMap.put(sk, fk);
                        fkskMap.put(fk, sk);
                        fkvMap.put(fk, getBySecondKey(sk));
                    }
                }
            }
        }
    }

    public void putByFirstKey(FK fk, V value) {
        // 首先找到fk映射的sk,如果sk不为null,则重新删除后put整体
        if (fk != null) {
            synchronized (this) {
                SK sk = fkskMap.get(fk);
                if (sk != null) {
                    put(fk, sk, value);
                } else {
                    fkvMap.put(fk, value);
                }
            }
        }
    }

    public void putBySecondKey(SK sk, V value) {
        // 首先找到sk映射到的fk,如果fk不为null,则重新删除后put整体
        if (sk != null) {
            synchronized (this) {
                FK fk = skfkMap.get(sk);
                if (fk != null) {
                    put(fk, sk, value);
                } else {
                    skvMap.put(sk, value);
                }
            }
        }
    }

    public void put(FK fk, SK sk, V value) {
        // 首先找到原来fk映射的sk和value,sk映射的fk和value,移除,然后重新插入
        if (fk != null && sk != null) {
            synchronized (this) {
                fkskMap.remove(fk);
                fkvMap.remove(fk);
                skfkMap.remove(sk);
                skvMap.remove(sk);
                fkskMap.put(fk, sk);
                fkvMap.put(fk, value);
                skfkMap.put(sk, fk);
                skvMap.put(sk, value);
            }
        }
    }

    public Set<FK> firstKeys() {
        synchronized (this) {
            Set<FK> result = new HashSet<>();
            result.addAll(fkskMap.keySet());
            result.addAll(fkvMap.keySet());
            return result;
        }
    }

    public Iterator<FK> iterateFirstKeys() {
        return firstKeys().iterator();
    }

    public Set<SK> secondKeys() {
        synchronized (this) {
            Set<SK> result = new HashSet<>();
            result.addAll(skfkMap.keySet());
            result.addAll(skvMap.keySet());
            return result;
        }
    }

    public Iterator<SK> iterateSecondKeys() {
        return secondKeys().iterator();
    }

    public Set<V> values() {
        synchronized (this) {
            Set<V> result = new HashSet<>();
            result.addAll(fkvMap.values());
            result.addAll(skvMap.values());
            return result;
        }
    }

    public Iterator<V> iterateValues() {
        return values().iterator();
    }
}
