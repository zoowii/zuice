package zuice.utils;

/**
 * Created by zoowii on 2015/3/3.
 */
public interface PendingAction {
    public void apply();
}
