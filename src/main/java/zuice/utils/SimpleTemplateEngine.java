package zuice.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 简单的模板引擎
 * 语法使用${abc}, $abc这种,变量名要求a-zA-Z或下划线开头,包含字母或数字或下划线,并且如果是$abc形式,右边需要是非\w字符或结束
 * $符号前面不能是\
 * 暂时不支持if, else, for等
 * Created by zoowii on 14/12/19.
 */
public class SimpleTemplateEngine {
    public static class TemplateContext extends HashMap<String, Object> {

    }

    private static class TemplateVar {
        public final String name;
        public final String text;

        public TemplateVar(String text) {
            this.text = text;
            if (text.startsWith("${")) {
                text = text.substring(2);
            }
            if (text.startsWith("$")) {
                text = text.substring(1);
            }
            if (text.endsWith("}")) {
                text = text.substring(0, text.length() - 1);
            }
            this.name = text;
        }

        /**
         * 可能多个变量连在一起
         *
         * @param text
         * @return
         */
        public static List<TemplateVar> create(String text) {
            List<TemplateVar> vars = new ArrayList<>();
            if (text == null || text.trim().length() < 1) {
                return vars;
            }
            Pattern varPattern = Pattern.compile("(?<!\\\\)\\$(\\{[a-zA-Z_]\\w*\\}|([a-zA-Z_]\\w*(?!\\w)))");
            Matcher varMatcher = varPattern.matcher(text);
            while (varMatcher.find()) {
                String group = varMatcher.group();
                vars.add(new TemplateVar(group));
            }
            return vars;
        }
    }

    public static class Template {
        private final String text;
        private final List<Object> items = new ArrayList<>();

        public Template(String text) {
            // 给模板后面添加一个空格,最后取出内容的时候要排除掉一个空格,这是为了在模板最后内容是变量的时候方便处理
            this.text = text + " ";
            splitTemplateByVars();
        }

        /**
         * 把模板的源文本中的变量部分剥离出来
         */
        private void splitTemplateByVars() {
            items.clear();
            String[] splited = text.split("(?<!\\\\)\\$(\\{[a-zA-Z_]\\w*\\}|([a-zA-Z_]\\w*(?!\\w)))");
            String leftText = text;
            // 当模板中只有变量时,splited为空,这种情况需要独立考虑
            if (splited.length < 1) {
                for (TemplateVar templateVar : TemplateVar.create(text)) {
                    items.add(templateVar);
                }
            }
            for (String itemText : splited) {
                if (leftText.startsWith(itemText)) {
                    items.add(itemText);
                    leftText = leftText.substring(itemText.length());
                } else {
                    int textIdx = leftText.indexOf(itemText);
                    if (textIdx < 0) {
                        throw new RuntimeException("split template vars error");
                    }
                    if (textIdx == 0) {
                        items.add(itemText);
                        leftText = leftText.substring(itemText.length());
                    } else {
                        String varText = leftText.substring(0, textIdx);
                        leftText = leftText.substring(textIdx + itemText.length());
                        for (TemplateVar templateVar : TemplateVar.create(varText)) {
                            items.add(templateVar);
                        }
                        items.add(itemText);
                    }
                }
            }

        }

        public Iterator<Object> iterateItems() {
            return items.iterator();
        }

        public String getText() {
            return text;
        }
    }

    public String render(String template, TemplateContext context) {
        if (template == null) {
            return null;
        }
        return render(new Template(template), context);
    }

    public String render(Template template, TemplateContext context) {
        if (template == null || context == null) {
            return template != null ? template.getText() : null;
        }
        StringBuilder builder = new StringBuilder();
        Iterator<Object> templateItemsIterator = template.iterateItems();
        while (templateItemsIterator.hasNext()) {
            Object item = templateItemsIterator.next();
            if (item instanceof String) {
                builder.append(item);
            } else if (item instanceof TemplateVar) {
                TemplateVar templateVar = (TemplateVar) item;
                if (context.containsKey(templateVar.name)) {
                    builder.append(context.get(templateVar.name));
                } else {
                    builder.append(templateVar.text);
                }
            } else {
                builder.append(item);
            }
        }
        String out = builder.toString();
        return out.length() > 0 ? out.substring(0, out.length() - 1) : out;
    }
}
