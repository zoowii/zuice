package zuice.utils;

/**
 * Created by zoowii on 14/11/1.
 */
public class StringUtils {
    public static boolean isEqual(String a, String b) {
        if (a == null && b == null) {
            return true;
        }
        if (a == null) {
            return false;
        }
        return a.equals(b);
    }

    public static boolean isEmpty(String str) {
        return str == null || str.trim().length() < 1;
    }

    /**
     * 去掉str中头尾的end字符串
     *
     * @param str
     * @param end
     * @return
     */
    public static String trim(String str, String end) {
        if (isEmpty(str) || isEmpty(end)) {
            return str;
        }
        if (str.endsWith(end)) {
            return str.substring(0, str.length() - end.length());
        }
        if (str.startsWith(end)) {
            return str.substring(end.length());
        }
        return str;
    }

    /**
     * 判断str是否以prefixes中任何一个为前缀
     *
     * @param str
     * @param prefixes
     * @return
     */
    public static boolean startsWith(String str, String[] prefixes) {
        if (str == null | prefixes == null || prefixes.length < 1) {
            return false;
        }
        for (String prefix : prefixes) {
            if (str.startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }
}
