package zuice.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by zoowii on 2014/11/2.
 */
public class TemplateUtil {
    private static SimpleTemplateEngine templateEngine = new SimpleTemplateEngine();

    private TemplateUtil() {
    }

    public static String render(String templateStr, Properties properties) throws IOException {
        Map<String, Object> binding = new HashMap<>();
        if (properties == null) {
            return render(templateStr, binding);
        }
        for (Object key : properties.keySet()) {
            binding.put(key.toString(), properties.getProperty(key.toString()));
        }
        return render(templateStr, binding);
    }

    public static String render(String templateStr, Map<String, Object> binding) throws IOException {
        if (templateStr == null) {
            return null;
        }
        Map<String, Object> tmplBinding = binding != null ? binding : new HashMap<String, Object>();
        SimpleTemplateEngine.TemplateContext context = new SimpleTemplateEngine.TemplateContext();
        for (String key : tmplBinding.keySet()) {
            context.put(key, tmplBinding.get(key));
        }
        return templateEngine.render(templateStr, context);
    }
}
