package zuice.utils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 产生tokenizer
 * <p/>
 * Usage:
 * List<TokenizerGenerator.Rule> rules = new ArrayList<>();
 * rules.add(new TokenizerGenerator.Rule("IDENTIFIER", "[a-zA-Z_\\$][a-zA-Z0-9_]*"));
 * rules.add(new TokenizerGenerator.Rule("NUMBER", "[+-]?(?:(0x[0-9a-fA-F]+|0[0-7]+)|((?:[0-9]+(?:\\.[0-9]*)?|\\.[0-9]+)(?:[eE][+-]?[0-9]+)?|NaN|Infinity))"));
 * rules.add(new TokenizerGenerator.Rule("WHITESPACE", "\\s+"));
 * rules.add(new TokenizerGenerator.Rule("ILLEGAL", "[.\\s]+"));
 * String text = "1234 567 abc";
 * Iterator<TokenizerGenerator.Token> tokenIterator = TokenizerGenerator.generateTokenStream(rules, text);
 * while (tokenIterator.hasNext()) {
 * TokenizerGenerator.Token token = tokenIterator.next();
 * System.out.println(token);
 * }
 * <p/>
 * FIXME: 在解析xml文本时有可能出BUG无法解析
 * Created by zoowii on 14/12/19.
 */
public class TokenizerGenerator {
    private static String joinString(List<String> items, String sep) {
        if (items == null || items.size() < 1) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < items.size(); ++i) {
            if (i > 0) {
                builder.append(sep);
            }
            builder.append(items.get(i));
        }
        return builder.toString();
    }

    private static String joinString(String[] items, String sep) {
        if (items == null) {
            return "";
        }
        List<String> itemsList = new ArrayList<>();
        for (String item : items) {
            itemsList.add(item);
        }
        return joinString(itemsList, sep);
    }

    public static class Rule {
        public final String pattern;
        public final String name;
        private int captureCount = 0;

        public Rule(String name, String pattern) {
            this.name = name;
            this.pattern = pattern;
        }

        public int getCaptureCount() {
            return captureCount;
        }

        public void setCaptureCount(int captureCount) {
            this.captureCount = captureCount;
        }
    }

    public static class Token {
        private String type;
        private String value;
        private String match;
        private int offset;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getMatch() {
            return match;
        }

        public void setMatch(String match) {
            this.match = match;
        }

        public int getOffset() {
            return offset;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }

        @Override
        public String toString() {
            return String.format("token: %s\t%s\t%s\t%d", type, value, match, offset);
        }
    }

    public static Iterator<Token> generateTokenStream(List<Rule> rules, String text) {
        if (rules == null || text == null) {
            return null;
        }
        List<String> tokenExpressions = new ArrayList<>();
        int captureCount = 0;
        for (Rule rule : rules) {
            String expression = rule.pattern;
            Pattern captureGroupPattern = Pattern.compile("\\\\.|\\[(?:\\\\.|[^\\]])*\\]|(\\((?!\\?[!:=]))|.");
            int groupsCount = 0;
            Matcher captureGroupMatcher = captureGroupPattern.matcher(expression);
            while (captureGroupMatcher.find()) {
                if (captureGroupMatcher.group(1) != null) {
                    groupsCount += 1;
                }
            }
            rule.setCaptureCount(groupsCount + 1);
            Pattern backRefExpPattern = Pattern.compile("\\\\\\D|\\[(?:\\\\.|[^\\]])*\\]|\\\\(\\d+)|.");
            Matcher backRefExpMatcher = backRefExpPattern.matcher(expression);
            StringBuilder backRefedExprBuilder = new StringBuilder();
            while (backRefExpMatcher.find()) {
                if (backRefExpMatcher.group(1) != null) {
                    String dStr = backRefExpMatcher.group(1);
                    int n = Integer.valueOf(dStr);
                    if (n > 0 && n <= groupsCount) {
                        backRefedExprBuilder.append("\\" + (n + captureCount + 1));
                    } else {
                        backRefedExprBuilder.append("" + Integer.valueOf(n + "", 8));
                    }
                } else {
                    backRefedExprBuilder.append(backRefExpMatcher.group());
                }
            }
            captureCount += groupsCount + 1;
            tokenExpressions.add(expression);
        }
        Pattern tokenRegPattern = Pattern.compile("(" + joinString(tokenExpressions, ")|(") + ")");
        int index = 0;
        List<Token> tokens = new ArrayList<>();
        Matcher textMatcher = tokenRegPattern.matcher(text);
        while (textMatcher.find()) {
            Token token = new Token();
            String group = textMatcher.group(0);
            assert group != null;
            token.setValue(group);
            token.setOffset(index);
            index += group.length();
            // 找到满足的是哪个rule
            for (Rule rule : rules) {
                Pattern rulePattern = Pattern.compile(rule.pattern);
                if (rulePattern.matcher(group).matches()) {
                    token.setType(rule.name);
                    break;
                }
            }
//            token.setMatch();
            tokens.add(token);
        }
        return tokens.iterator();
    }
}
