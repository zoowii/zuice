package zuice.test;

import zuice.annotations.Component;

/**
 * Created by zoowii on 14/11/1.
 */
public interface IAgeService {
    public int getAge(String name);
}
