package zuice.test;

/**
 * Created by zoowii on 14/10/31.
 */
public interface IHelloService {
    public String hello(String name);
}
