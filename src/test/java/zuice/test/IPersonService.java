package zuice.test;

/**
 * Created by zoowii on 14/10/31.
 */
public interface IPersonService {
    public String getDescription(String name);
}
