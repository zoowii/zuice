package zuice.test;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import zuice.BeanFactory;
import zuice.config.xml.ClassPathXmlConfigLoader;

import javax.inject.Provider;
import java.io.InputStream;

/**
 * Created by zoowii on 14/11/1.
 */
public class InjectorTest {
    public static final Logger LOG = Logger.getLogger(InjectorTest.class);

    @Test
    public void testInjector() {
        BeanFactory injector = ClassPathXmlConfigLoader.createInjector("test_beans.xml"); // Injector.newInstance();
//        injector.addBeanBind(HelloService.class);
//        injector.addBeanBind(PersonService.class);
//        injector.addBeanBind("age_service", AgeService.class);
//        injector.scanPackage("zuice.test.impl");
        Provider<IHelloService> helloProvider = injector.getProvider(IHelloService.class);
        IHelloService helloService = helloProvider.get();
        System.out.println(helloService.hello("world"));
        Assert.assertTrue(helloService.hello("world").length() > 0);
        Assert.assertEquals(injector.getBean(InputStream.class), null);
        injector.close();
    }
}
