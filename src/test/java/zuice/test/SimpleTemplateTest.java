package zuice.test;

import org.junit.Test;
import zuice.utils.SimpleTemplateEngine;
import zuice.utils.TokenizerGenerator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by zoowii on 14/12/19.
 */
public class SimpleTemplateTest {

    @Test
    public void testRegex() {
        String regex = "abc|(\\d+)|.";
        String regex2 = "\\\\.|\\[(?:\\\\.|[^\\]])*\\]|(\\((?!\\?[!:=]))|.";
        Pattern pattern = Pattern.compile(regex2);
        String text = "abc 123 def 3424";
        String text2 = "[+-]?(?:(0x[0-9a-fA-F]+|0[0-7]+)|((?:[0-9]+(?:\\.[0-9]*)?|\\.[0-9]+)(?:[eE][+-]?[0-9]+)?|NaN|Infinity))";
        Matcher matcher = pattern.matcher(text2);
        int matchedGroupCount = 0;
        while (matcher.find()) {
            String group = matcher.group();
            int groupCount = matcher.groupCount();
            if (matcher.group(1) != null) {
                matchedGroupCount += 1;
            }
        }
        System.out.println("matched count is " + matchedGroupCount);
    }

    @Test
    public void testTokenizerGenerator() {
        List<TokenizerGenerator.Rule> rules = new ArrayList<>();
        rules.add(new TokenizerGenerator.Rule("IDENTIFIER", "[a-zA-Z_\\$][a-zA-Z0-9_]*"));
        rules.add(new TokenizerGenerator.Rule("NUMBER", "[+-]?(?:(0x[0-9a-fA-F]+|0[0-7]+)|((?:[0-9]+(?:\\.[0-9]*)?|\\.[0-9]+)(?:[eE][+-]?[0-9]+)?|NaN|Infinity))"));
        rules.add(new TokenizerGenerator.Rule("WHITESPACE", "\\s+"));
        rules.add(new TokenizerGenerator.Rule("ILLEGAL", "[.\\s]+"));
        String text = "1234 567 abc";
        Iterator<TokenizerGenerator.Token> tokenIterator = TokenizerGenerator.generateTokenStream(rules, text);
        while (tokenIterator.hasNext()) {
            TokenizerGenerator.Token token = tokenIterator.next();
            System.out.println(token);
        }
    }

    @Test
    public void testTemplateVars() {
        SimpleTemplateEngine templateEngine = new SimpleTemplateEngine();
        String tmpl = "<xml><name>$name</name><age>${age}</age><desc>name is ${name} and age is $age, that's ${name}$age, and demo of var is \\$age and $not_found_var.</desc></xml>";
//        String tmpl = "   ${name}${age}";
        SimpleTemplateEngine.TemplateContext context1 = new SimpleTemplateEngine.TemplateContext();
        context1.put("name", "zoowii");
        context1.put("age", 24);
        String tmpl1Out = templateEngine.render(tmpl, context1);
        System.out.println(tmpl1Out);
    }
}
