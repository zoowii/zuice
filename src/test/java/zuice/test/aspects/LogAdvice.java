package zuice.test.aspects;

import org.apache.log4j.Logger;
import zuice.aop.JoinPoint;
import zuice.aop.annotations.After;
import zuice.aop.annotations.Aspect;
import zuice.aop.annotations.Before;
import zuice.aop.annotations.Pointcut;

import javax.inject.Singleton;

/**
 * Created by zoowii on 14/11/4.
 */
@Singleton
@Aspect
public class LogAdvice {
    private static final Logger LOG = Logger.getLogger(LogAdvice.class);

//    @SuppressWarnings("all")
//    @Pointcut("zuice.test.impl.*")
    public void logPoint() {
    }

    @Before("logPoint()")
    public void beforeService(JoinPoint joinPoint) {
        System.out.println("before log");
    }

    @After("logPoint()")
    public void afterService(JoinPoint joinPoint) {
        System.out.println("after log");
    }
}
