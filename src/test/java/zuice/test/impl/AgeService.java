package zuice.test.impl;

import zuice.BeanFactory;
import zuice.annotations.Autowired;
import zuice.annotations.Component;
import zuice.annotations.Qualified;
import zuice.test.AbstractAgeService;
import zuice.test.IAgeService;
import zuice.test.IPersonService;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by zoowii on 14/11/1.
 */
@Component("age_service")
@Singleton
public class AgeService extends AbstractAgeService {

    @Autowired
    private IPersonService personService;

    @Inject
    public AgeService(BeanFactory beanFactory) {
        assert beanFactory != null;
    }

    public void init() {
        System.out.println("age service inited");
    }

    @Override
    public int getAge(String name) {
        return 23;
    }

    public void close() {
        System.out.println("the age service closed");
    }
}
