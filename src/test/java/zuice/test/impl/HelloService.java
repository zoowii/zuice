package zuice.test.impl;

import zuice.annotations.Autowired;
import zuice.annotations.Component;
import zuice.annotations.InjectScope;
import zuice.annotations.Qualified;
import zuice.test.IHelloService;
import zuice.test.IPersonService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.inject.Singleton;

/**
 * Created by zoowii on 14/10/31.
 */
@Component
@InjectScope
public class HelloService implements IHelloService {
    @Autowired
    private IPersonService personService;

    @Inject
    public HelloService(@Qualified("personService") Provider<IPersonService> personServiceProvider) {
        System.out.println("constructor of HelloService, ");
        System.out.println(personServiceProvider.get().getDescription("zoowii"));
    }

    @Override
    public String hello(String name) {
        return String.format("Hello, %s, \nand description is %s", name, personService.getDescription(name));
    }
}
