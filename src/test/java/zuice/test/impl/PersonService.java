package zuice.test.impl;

import zuice.BeanFactory;
import zuice.annotations.Autowired;
import zuice.annotations.Component;
import zuice.annotations.Qualified;
import zuice.annotations.ThreadScope;
import zuice.test.AbstractAgeService;
import zuice.test.IAgeService;
import zuice.test.IHelloService;
import zuice.test.IPersonService;

import javax.inject.Inject;

/**
 * Created by zoowii on 14/10/31.
 */
@Component
@ThreadScope
public class PersonService implements IPersonService {
    @Inject
    private String family;

    //    @Qualified("age_service")
    @Autowired
    private AbstractAgeService ageService2;

    public PersonService(int num) {
        System.out.println("test num " + num);
    }

    @Override
    public String getDescription(String name) {
        return "This is " + name + "'s description, and your age is " + ageService2.getAge(name) + " and my family is " + family;
    }
}
